package com.coursemanagement.FinalProject.service;

import com.coursemanagement.FinalProject.dto.requestDto.ProfessorUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.professor.ProfessorResponseDto;
import com.coursemanagement.FinalProject.entity.Professor;
import com.coursemanagement.FinalProject.repository.ProfessorRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
//import sun.security.mscapi.PRNG;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ProfessorServiceImpTest {

    @Mock
    private ProfessorRepository professorRepository;
    @Mock
    private DepartmentServiceImp departmentServiceImp;
    @Mock
    private CourseServiceImp courseServiceImp;
    private ProfessorServiceImp professorServiceImp;
    private ProfessorResponseDto professorResponseDto;
    private ProfessorUpdateRequestDto professorUpdateRequestDto;
    private AutoCloseable autoCloseable;
    private Professor professor;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        professorServiceImp = new ProfessorServiceImp(professorRepository, departmentServiceImp, courseServiceImp);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void itShouldReturnProfessorWithId() {
        professor = new Professor(85589L, "shayan", "shahini", 11111L, "3333");
        professor.setProfessorId(1L);
        when(professorRepository.findById(professor.getProfessorId())).thenReturn(Optional.ofNullable(professor));
        professorResponseDto = professorServiceImp.getProfessor(professor.getProfessorId());
        assertNotNull(professorResponseDto);
        assertEquals("shayan", professorResponseDto.getProfessorFirstName());
    }

    @Test
    void updateProfessor() {
        professor = new Professor(77777L, "akbar", "shoja", 22222L, "5555");
        professor.setProfessorId(1L);
        professorUpdateRequestDto = new ProfessorUpdateRequestDto(85589L, "ali", "shahini", 11111L);
        when(professorRepository.findById(professor.getProfessorId())).thenReturn(Optional.ofNullable(professor));
        when(professorRepository.findAll()).thenReturn(
                new ArrayList<Professor>(Collections.singletonList(professor))
        );
        when((professorRepository.save(Mockito.any(Professor.class)))).thenReturn(professor);
        professorResponseDto = professorServiceImp.updateProfessor(professor.getProfessorId(), professorUpdateRequestDto);
        assertEquals("ali", professorUpdateRequestDto.getProfessorFirstName());
    }
}