package com.coursemanagement.FinalProject.repository;

import com.coursemanagement.FinalProject.entity.Department;
import com.coursemanagement.FinalProject.entity.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
