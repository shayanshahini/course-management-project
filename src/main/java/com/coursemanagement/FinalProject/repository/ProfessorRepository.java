package com.coursemanagement.FinalProject.repository;

import com.coursemanagement.FinalProject.entity.Department;
import com.coursemanagement.FinalProject.entity.Professor;
import com.coursemanagement.FinalProject.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor, Long> {
    List<Professor> findByProfessorNationalId(Long professorNationalId);
}
