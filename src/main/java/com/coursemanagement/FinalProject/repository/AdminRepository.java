package com.coursemanagement.FinalProject.repository;

import com.coursemanagement.FinalProject.entity.Admin;
import com.coursemanagement.FinalProject.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Long> {
    List<Admin> findByAdminNationalId(Long adminNationalId);
}
