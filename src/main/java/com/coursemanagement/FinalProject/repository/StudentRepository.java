package com.coursemanagement.FinalProject.repository;

import com.coursemanagement.FinalProject.entity.Course;
import com.coursemanagement.FinalProject.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    List<Student> findByStudentNationalId(Long studentNationalId);
}
