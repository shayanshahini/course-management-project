package com.coursemanagement.FinalProject.repository;

import com.coursemanagement.FinalProject.entity.Course;
import com.coursemanagement.FinalProject.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
}
