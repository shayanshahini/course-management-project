//package com.coursemanagement.FinalProject.config;
//
//import com.coursemanagement.FinalProject.entity.Admin;
//import com.coursemanagement.FinalProject.entity.Professor;
//import com.coursemanagement.FinalProject.entity.Student;
//import com.coursemanagement.FinalProject.repository.AdminRepository;
//import com.coursemanagement.FinalProject.repository.ProfessorRepository;
//import com.coursemanagement.FinalProject.repository.StudentRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Service
//public class CustomUserDetails implements UserDetailsService {
//    @Autowired
//    private StudentRepository studentRepository;
//
//    @Autowired
//    private ProfessorRepository professorRepository;
//
//    @Autowired
//    private AdminRepository adminRepository;
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        String userName = null;
//        String password = null;
//        List<GrantedAuthority> authorities = null;
//        Long nationalId = Long.parseLong(username);
//        Student student = null;
//        Professor professor = null;
//        Admin admin = null;
//
//        List<Student> students = studentRepository.findByStudentNationalId(nationalId);
//        if (!students.isEmpty()) {
//            student = students.get(0);
//        }
//
//        if (student == null && admin == null) {
//            List<Professor> professors = professorRepository.findByProfessorNationalId(nationalId);
//            if (!professors.isEmpty()) {
//                professor = professors.get(0);
//            }
//        }
//
//        if (student == null && professor == null) {
//            List<Admin> admins = adminRepository.findByAdminNationalId(nationalId);
//            if (!admins.isEmpty()) {
//                admin = admins.get(0);
//            }
//        }
//
//
//        if (student == null && professor == null && admin == null) {
//            throw new UsernameNotFoundException("User details not found for the user: " + username);
//        } else {
//            if (professor == null && admin == null) {
//                userName = String.valueOf(student.getStudentNationalId());
//                password = student.getPassword();
//                authorities = new ArrayList<>();
//                authorities.add(new SimpleGrantedAuthority(student.getRole()));
//            } else if (student == null && admin == null) {
//                userName = String.valueOf(professor.getProfessorNationalId());
//                password = professor.getPassword();
//                authorities = new ArrayList<>();
//                authorities.add(new SimpleGrantedAuthority(professor.getRole()));
//            } else if (student == null && professor == null) {
//                userName = String.valueOf(admin.getAdminNationalId());
//                password = admin.getPassword();
//                authorities = new ArrayList<>();
//                authorities.add(new SimpleGrantedAuthority(admin.getRole()));
//            }
//
//                return new User(userName, password, authorities);
//        }
//    }
//
//}
//
//
