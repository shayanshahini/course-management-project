package com.coursemanagement.FinalProject.service;

import com.coursemanagement.FinalProject.dto.requestDto.StudentAddRequestDto;
import com.coursemanagement.FinalProject.dto.requestDto.StudentUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.professor.ProfessorResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.student.StudentResponseDto;
import com.coursemanagement.FinalProject.entity.Student;

import java.util.List;

public interface StudentService {
    public StudentResponseDto getStudent(Long id);
    public StudentResponseDto getStudentByNationalId(Long nationalID);
    public Student getStudentByNaId(Long nationalID);
    public Student getStudentById(Long id);
    public List<StudentResponseDto> getStudents();
    public List<StudentResponseDto> getStudentsWithPagination(int offset, int pageSize);
    public StudentResponseDto addStudent(StudentAddRequestDto studentAddRequestDto);
    public Student deleteById(Long id);
    public Student deleteByNationalId(Long nationalId);
    public StudentResponseDto updateStudent(Long id, StudentUpdateRequestDto studentUpdateRequestDto);
    public StudentResponseDto updateStudentByNationalId(Long nationalId, StudentUpdateRequestDto studentUpdateRequestDto);
    public StudentResponseDto chooseCourse(Long studentId, Long courseId, Double grade);
    public StudentResponseDto chooseCourseByStudentNationalId(Long studentNationalId, Long courseId, Double grade);
    public double studentAverage(Long studentId);
    public double studentAverageByNationalId(Long studentNationalId);


}
