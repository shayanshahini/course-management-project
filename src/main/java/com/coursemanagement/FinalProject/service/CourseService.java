package com.coursemanagement.FinalProject.service;

import com.coursemanagement.FinalProject.dto.requestDto.CourseAddRequestDto;
import com.coursemanagement.FinalProject.dto.requestDto.CourseUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.course.CourseResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.student.StudentResponseDto;
import com.coursemanagement.FinalProject.entity.Course;

import java.util.List;

public interface CourseService {
    public CourseResponseDto getCourse(Long id);
    public Course getCourseById(Long id);
    public List<CourseResponseDto> getCourses();
    public List<CourseResponseDto> getCoursesWithPagination(int offset, int pageSize);
    public CourseResponseDto addCourse(CourseAddRequestDto courseAddRequestDto);
    public Course deleteById(Long id);
    public CourseResponseDto updateCourse(Long id, CourseUpdateRequestDto courseUpdateRequestDto);
    public CourseResponseDto chooseStudent(Long courseId, Long studentId, Double grade);




}
