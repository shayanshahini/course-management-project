package com.coursemanagement.FinalProject.service;

import com.coursemanagement.FinalProject.dto.mapping.deprecated.StudentEntityToDto;
import com.coursemanagement.FinalProject.dto.requestDto.StudentAddRequestDto;
import com.coursemanagement.FinalProject.dto.requestDto.StudentUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.student.StudentResponseDto;
import com.coursemanagement.FinalProject.entity.*;
import com.coursemanagement.FinalProject.entity.utility.EnrollmentId;
import com.coursemanagement.FinalProject.dto.mapping.reflection.Mapper;
import com.coursemanagement.FinalProject.repository.EnrollmentRepository;
import com.coursemanagement.FinalProject.repository.StudentRepository;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class StudentServiceImp implements StudentService {
    private StudentRepository studentRepository;
    private CourseServiceImp courseServiceImp;
    private DepartmentServiceImp departmentServiceImp;
    private EnrollmentRepository enrollmentRepository;

//    @Autowired
//    private PasswordEncoder passwordEncoder;

    @Autowired
    public StudentServiceImp(StudentRepository studentRepository,
                             @Lazy CourseServiceImp courseServiceImp,
                             @Lazy DepartmentServiceImp departmentServiceImp,
                             EnrollmentRepository enrollmentRepository) {
        this.studentRepository = studentRepository;
        this.courseServiceImp = courseServiceImp;
        this.departmentServiceImp = departmentServiceImp;
        this.enrollmentRepository = enrollmentRepository;
    }

    public StudentResponseDto getStudent(Long id) {
        Student student = studentRepository.findById(id).orElse(null);
        return StudentEntityToDto.studentToDto(student);
    }

    @Override
    public StudentResponseDto getStudentByNationalId(Long studentNationalId) {
        Student student = studentRepository.findByStudentNationalId(studentNationalId).get(0);
        Mapper mapper = new Mapper();
        return (StudentResponseDto) mapper.entityToDto(student, StudentResponseDto.class);
    }

    @Override
    public Student getStudentByNaId(Long nationalID) {
        Student student = studentRepository.findByStudentNationalId(nationalID).get(0);
        if (student.getStudentId() > 0)
            return student;
        else
            throw  new RuntimeException("This Student With This University Id Isn't Defined!");
    }

    public Student getStudentById(Long id) {
        Student student = studentRepository.findById(id).orElse(null);
        return student;
    }

    public List<StudentResponseDto> getStudents() {
        List<Student> students = StreamSupport
                .stream((studentRepository.findAll().spliterator()), false)
                .collect(Collectors.toList());
        Mapper mapper = new Mapper();
        return mapper.entitiesToDtos(students, StudentResponseDto.class);
    }

    @Override
    public List<StudentResponseDto> getStudentsWithPagination(int offset, int size) {
        List<Student> students = studentRepository.findAll(PageRequest.of(offset, size)).getContent();
        Mapper mapper = new Mapper();
        return mapper.entitiesToDtos(students, StudentResponseDto.class);
    }

    @Override
    @Transactional
    public StudentResponseDto addStudent(StudentAddRequestDto studentAddRequestDto) {
        Student student = new Student();
        for (Student st : studentRepository.findAll()) {
            if (studentAddRequestDto.getStudentUniversityId().equals(st.getStudentUniversityId()))
                throw new RuntimeException("This Student With This University Id Is Already Defined!!");
        }

        if (studentAddRequestDto.getStudentUniversityId() != null) {
            for (Student student1 : studentRepository.findAll()) {
                if (studentAddRequestDto.getStudentUniversityId().equals(student1.getStudentUniversityId())) {
                    throw new RuntimeException("This student university id has been defined before!!");
                }
            }
            student.setStudentUniversityId(studentAddRequestDto.getStudentUniversityId());
        }
        else
            throw new RuntimeException("Student Doesn't Have University Id!!");

        if (studentAddRequestDto.getStudentFirstName() != null)
            student.setStudentFirstName(studentAddRequestDto.getStudentFirstName());
        else
            throw new RuntimeException("Student Doesn't Have First Name!!");

        if (studentAddRequestDto.getStudentLastName() != null)
            student.setStudentLastName(studentAddRequestDto.getStudentLastName());
        else
            throw new RuntimeException("Student Doesn't Have Last Name!!");

        if (studentAddRequestDto.getStudentNationalId() != null) {
            for (Student student1 : studentRepository.findAll()) {
                if (studentAddRequestDto.getStudentNationalId().equals(student1.getStudentNationalId())) {
                    throw new RuntimeException("This student national id has been defined before!!");
                }
            }
            student.setStudentNationalId(studentAddRequestDto.getStudentNationalId());
        }
        else
            throw new RuntimeException("Student Doesn't Have National Id!!");

        if (studentAddRequestDto.getPassword() != null) {
//            String hash = passwordEncoder.encode(studentAddRequestDto.getPassword());
            student.setPassword(studentAddRequestDto.getPassword());
        } else
            throw new RuntimeException("Student Doesn't Have Password!!");

        if (studentAddRequestDto.getAddress() != null)
            student.setAddress(studentAddRequestDto.getAddress());

        if (studentAddRequestDto.getDepartment() != null) {
            Department department = departmentServiceImp.getDepartmentById(studentAddRequestDto.getDepartment());
            student.setDepartment(department);
        }

        if (studentAddRequestDto.getCourseWithGrade() != null) {
            studentAddRequestDto.getCourseWithGrade().forEach(
                    (k, v) -> {
                        Course course = courseServiceImp.getCourseById(k);
                        if (course.getProfessor() == null)
                            throw new RuntimeException("This CourseDoesn't Have Professor. Please First Set a Professor for this Course!!");
                        student.addEnrollment(
                                new Enrollment(
                                        new EnrollmentId(student.getStudentId(), course.getCourseId()),
                                        student, course, v));
                    }
            );
        }

        studentRepository.save(student);
        Mapper mapper = new Mapper();
        return (StudentResponseDto) mapper.entityToDto(student, StudentResponseDto.class);
    }

    @Override
    @Transactional
    public Student deleteById(Long id) {
        Student student = studentRepository.findById(id).orElse(null);

        if (student != null) {
            studentRepository.delete(student);
            return student;
        } else {
            throw new RuntimeException("Student with ID " + id + " not found");
        }
    }
    @Override
    @Transactional
    public Student deleteByNationalId(Long studentNationalId) {
        Student student = getStudentByNaId(studentNationalId);
        studentRepository.delete(student);
        return student;
    }


    @Override
    @Transactional
    public StudentResponseDto updateStudent(Long id, StudentUpdateRequestDto studentUpdateRequestDto) {
        Student student = studentRepository.findById(id).orElse(null);
        boolean checkExist = false;
        for (Student st : studentRepository.findAll()) {
            if (st.getStudentId() == id) {
                checkExist = true;
                student = st;
                break;
            }
        }

        if (checkExist == true) {
            if (studentUpdateRequestDto.getStudentUniversityId() != null) {
                for (Student student1 : studentRepository.findAll()) {
                    if (studentUpdateRequestDto.getStudentUniversityId().equals(student1.getStudentUniversityId())) {
                        throw new RuntimeException("This university id has been defined before!!");
                    }
                }
                student.setStudentUniversityId(studentUpdateRequestDto.getStudentUniversityId());
            }


            if (studentUpdateRequestDto.getStudentFirstName() != null)
                student.setStudentFirstName(studentUpdateRequestDto.getStudentFirstName());

            if (studentUpdateRequestDto.getStudentLastName() != null)
                student.setStudentLastName(studentUpdateRequestDto.getStudentLastName());

            if (studentUpdateRequestDto.getStudentNationalId() != null) {
                for (Student student1 : studentRepository.findAll()) {
                    if (studentUpdateRequestDto.getStudentNationalId().equals(student1.getStudentNationalId())) {
                        throw new RuntimeException("This student national id has been defined before!!");
                    }
                }
                student.setStudentNationalId(studentUpdateRequestDto.getStudentNationalId());
            }

            if (studentUpdateRequestDto.getPassword() != null) {
//                String hash = passwordEncoder.encode(studentUpdateRequestDto.getPassword());
                student.setPassword(studentUpdateRequestDto.getPassword());
            }


            if (studentUpdateRequestDto.getAddress() != null)
                student.setAddress(studentUpdateRequestDto.getAddress());

            if (studentUpdateRequestDto.getDepartment() != null) {
                Department department = departmentServiceImp.getDepartmentById(studentUpdateRequestDto.getDepartment());
                student.setDepartment(department);
            }
            if (studentUpdateRequestDto.getCourseWithGrade() != null) {
                List<Enrollment> enrollments = new ArrayList<>(student.getEnrollments());
                List<Enrollment> oldCourse = new ArrayList<>();
                for (Enrollment enrollment : enrollments) {
                    studentUpdateRequestDto.getCourseWithGrade().forEach(
                            (course, grade) -> {
                                if (enrollment.getCourse().getCourseId() == course && !enrollment.getGrade().equals(grade)) {
                                    enrollment.setGrade(grade);
                                    enrollmentRepository.save(enrollment);
                                    oldCourse.add(enrollment);
                                } else if (enrollment.getCourse().getCourseId() == course && enrollment.getGrade().equals(grade)) {
                                    oldCourse.add(enrollment);
                                }
                            }
                    );
                    if (!oldCourse.contains(enrollment)) {
                        student.removeEnrollment(enrollment);
                        enrollmentRepository.delete(enrollment);
                    }
                }
                studentRepository.save(student);
                Student finalStudent = student;
                studentUpdateRequestDto.getCourseWithGrade().forEach(
                        (k, v) -> {
                            Course course = courseServiceImp.getCourseById(k);
                            finalStudent.addEnrollment(
                                    new Enrollment(
                                            new EnrollmentId(finalStudent.getStudentId(), course.getCourseId()),
                                            finalStudent, course, v));
                        }
                );
                student = finalStudent;
            }
            studentRepository.save(student);
            Mapper mapper = new Mapper();
            return (StudentResponseDto) mapper.entityToDto(student, StudentResponseDto.class);
        } else
            throw new RuntimeException("Student With This Id Doesn't Exist!!");
    }

    @Override
    @Transactional
    public StudentResponseDto updateStudentByNationalId(Long studentNationalId, StudentUpdateRequestDto studentUpdateRequestDto) {
        Student student = getStudentByNaId(studentNationalId);
        if (student.getStudentId() > 0) {
            boolean checkExist = false;
            for (Student st : studentRepository.findAll()) {
                if (st.getStudentNationalId().equals(studentNationalId)) {
                    checkExist = true;
                    student = st;
                    break;
                }
            }

            if (checkExist == true) {
                if (studentUpdateRequestDto.getStudentUniversityId() != null) {
                    for (Student student1 : studentRepository.findAll()) {
                        if (studentUpdateRequestDto.getStudentUniversityId().equals(student1.getStudentUniversityId())) {
                            throw new RuntimeException("This university id has been defined before!!");
                        }
                    }
                    student.setStudentUniversityId(studentUpdateRequestDto.getStudentUniversityId());
                }


                if (studentUpdateRequestDto.getStudentFirstName() != null)
                    student.setStudentFirstName(studentUpdateRequestDto.getStudentFirstName());

                if (studentUpdateRequestDto.getStudentLastName() != null)
                    student.setStudentLastName(studentUpdateRequestDto.getStudentLastName());

                if (studentUpdateRequestDto.getStudentNationalId() != null) {
                    for (Student student1 : studentRepository.findAll()) {
                        if (studentUpdateRequestDto.getStudentNationalId().equals(student1.getStudentNationalId())) {
                            throw new RuntimeException("This student national id has been defined before!!");
                        }
                    }
                    student.setStudentNationalId(studentUpdateRequestDto.getStudentNationalId());
                }

                if (studentUpdateRequestDto.getPassword() != null) {
//                    String hash = passwordEncoder.encode(studentUpdateRequestDto.getPassword());
                    student.setPassword(studentUpdateRequestDto.getPassword());
                }


                if (studentUpdateRequestDto.getAddress() != null)
                    student.setAddress(studentUpdateRequestDto.getAddress());

                if (studentUpdateRequestDto.getDepartment() != null) {
                    Department department = departmentServiceImp.getDepartmentById(studentUpdateRequestDto.getDepartment());
                    student.setDepartment(department);
                }
                if (studentUpdateRequestDto.getCourseWithGrade() != null) {
                    List<Enrollment> enrollments = new ArrayList<>(student.getEnrollments());
                    List<Enrollment> oldCourse = new ArrayList<>();
                    for (Enrollment enrollment : enrollments) {
                        studentUpdateRequestDto.getCourseWithGrade().forEach(
                                (course, grade) -> {
                                    if (enrollment.getCourse().getCourseId() == course && !enrollment.getGrade().equals(grade)) {
                                        enrollment.setGrade(grade);
                                        enrollmentRepository.save(enrollment);
                                        oldCourse.add(enrollment);
                                    } else if (enrollment.getCourse().getCourseId() == course && enrollment.getGrade().equals(grade)) {
                                        oldCourse.add(enrollment);
                                    }
                                }
                        );
                        if (!oldCourse.contains(enrollment)) {
                            student.removeEnrollment(enrollment);
                            enrollmentRepository.delete(enrollment);
                        }
                    }
                    studentRepository.save(student);
                    Student finalStudent = student;
                    studentUpdateRequestDto.getCourseWithGrade().forEach(
                            (k, v) -> {
                                Course course = courseServiceImp.getCourseById(k);
                                finalStudent.addEnrollment(
                                        new Enrollment(
                                                new EnrollmentId(finalStudent.getStudentId(), course.getCourseId()),
                                                finalStudent, course, v));
                            }
                    );
                    student = finalStudent;
                }
                studentRepository.save(student);
                Mapper mapper = new Mapper();
                return (StudentResponseDto) mapper.entityToDto(student, StudentResponseDto.class);
            } else
                throw new RuntimeException("Student With This Id Doesn't Exist!!");
        } else
            throw new RuntimeException("Student With This Id Doesn't Exist!!!!!!");
    }

    @Override
    @Transactional
    public StudentResponseDto chooseCourse(Long studentId, Long courseId, Double grade) {
        Student student = getStudentById(studentId);
        Course course = courseServiceImp.getCourseById(courseId);
        List<Enrollment> enrollments = student.getEnrollments();
        EnrollmentId enrollmentId = new EnrollmentId();
        enrollmentId.setStudentId(studentId);
        enrollmentId.setCourseId(courseId);
        boolean check = false;
        for (Enrollment enrollment : enrollments) {
            if (enrollment.getEnrollmentId().equals(enrollmentId)) {
                check = true;
            }
        }
        if (check == false) {
            student.addEnrollment(new Enrollment(
                    new EnrollmentId(student.getStudentId(), course.getCourseId()), student, course, grade));
            studentRepository.save(student);
            Mapper mapper = new Mapper();
            return (StudentResponseDto) mapper.entityToDto(student, StudentResponseDto.class);
        } else
            throw new RuntimeException("The Student Has This Course!!");
    }

    @Override
    @Transactional
    public StudentResponseDto chooseCourseByStudentNationalId(Long studentNationalId, Long courseId, Double grade) {
        Student student = getStudentByNaId(studentNationalId);
        Course course = courseServiceImp.getCourseById(courseId);
        List<Enrollment> enrollments = student.getEnrollments();
        EnrollmentId enrollmentId = new EnrollmentId();
        enrollmentId.setStudentId(student.getStudentId());
        enrollmentId.setCourseId(courseId);
        boolean check = false;
        for (Enrollment enrollment : enrollments) {
            if (enrollment.getEnrollmentId().equals(enrollmentId)) {
                check = true;
            }
        }
        if (check == false) {
            student.addEnrollment(new Enrollment(
                    new EnrollmentId(student.getStudentId(), course.getCourseId()), student, course, grade));
            studentRepository.save(student);
            Mapper mapper = new Mapper();
            return (StudentResponseDto) mapper.entityToDto(student, StudentResponseDto.class);
        } else
            throw new RuntimeException("The Student Has This Course!!");
    }

    @Override
    public double studentAverage(Long studentId) {
        Student student = studentRepository.findById(studentId).orElse(null);
        List<Enrollment> enrollments = student.getEnrollments();
        if (!enrollments.isEmpty()) {
            double sum = 0;
            int counter = 0;
            Double average = null;
            for (Enrollment enrollment : enrollments) {
                sum += enrollment.getGrade();
                counter++;
            }
            average = sum / counter;
            return average;
        } else
            throw  new RuntimeException("This student doesn't have any courses!!");
    }

    @Override
    public double studentAverageByNationalId(Long studentNationalId) {
        Student student = getStudentByNaId(studentNationalId);
        List<Enrollment> enrollments = student.getEnrollments();
        if (!enrollments.isEmpty()) {
            double sum = 0;
            int counter = 0;
            Double average = null;
            for (Enrollment enrollment : enrollments) {
                sum += enrollment.getGrade();
                counter++;
            }
            average = sum / counter;
            return average;
        } else
            throw  new RuntimeException("This student doesn't have any courses!!");
    }
}
