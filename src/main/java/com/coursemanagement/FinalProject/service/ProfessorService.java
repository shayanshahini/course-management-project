package com.coursemanagement.FinalProject.service;

import com.coursemanagement.FinalProject.dto.requestDto.ProfessorAddRequestDto;
import com.coursemanagement.FinalProject.dto.requestDto.ProfessorUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.department.DepartmentResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.professor.ProfessorResponseDto;
import com.coursemanagement.FinalProject.entity.Professor;

import java.util.List;

public interface ProfessorService {
    public ProfessorResponseDto getProfessor(Long id);
    public ProfessorResponseDto getProfessorByNationalId(Long nationalId);
    public Professor getProfessorById(Long id);
    public List<ProfessorResponseDto> getProfessors();
    public List<ProfessorResponseDto> getProfessorsWithPagination(int offset, int pageSize);
    public ProfessorResponseDto addProfessor(ProfessorAddRequestDto professorAddRequestDto);
    public Professor deleteById(Long id);
    public Professor deleteByIdNationalId(Long nationalId);
    public ProfessorResponseDto updateProfessor(Long id, ProfessorUpdateRequestDto professorUpdateRequestDto);
    public ProfessorResponseDto updateProfessorByNationalId(Long nationalID, ProfessorUpdateRequestDto professorUpdateRequestDto);
    public ProfessorResponseDto chooseCourse(Long professorId, Long courseId);
    public ProfessorResponseDto chooseCourseByProfessorNationalId(Long professorNationalId, Long courseId);
    public double courseAverage(Long professorId, Long courseId);
    public double courseAverageByProfessorNationalId(Long professorNationalId, Long courseId);



}
