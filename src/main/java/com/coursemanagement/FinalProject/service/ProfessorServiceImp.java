package com.coursemanagement.FinalProject.service;

import com.coursemanagement.FinalProject.dto.requestDto.ProfessorAddRequestDto;
import com.coursemanagement.FinalProject.dto.requestDto.ProfessorUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.professor.ProfessorResponseDto;
import com.coursemanagement.FinalProject.entity.*;
import com.coursemanagement.FinalProject.dto.mapping.reflection.Mapper;
import com.coursemanagement.FinalProject.repository.ProfessorRepository;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ProfessorServiceImp implements ProfessorService {
    private ProfessorRepository professorRepository;
    private DepartmentServiceImp departmentServiceImp;
    private CourseServiceImp courseServiceImp;

//    @Autowired
//    private PasswordEncoder passwordEncoder;

    @Autowired
    public ProfessorServiceImp(ProfessorRepository professorRepository,
                               @Lazy DepartmentServiceImp departmentServiceImp,
                               CourseServiceImp courseServiceImp) {
        this.professorRepository = professorRepository;
        this.departmentServiceImp = departmentServiceImp;
        this.courseServiceImp = courseServiceImp;
    }

    public ProfessorResponseDto getProfessor(Long id) {
        Professor professor = professorRepository.findById(id).orElse(null);
        Mapper mapper = new Mapper();
        return (ProfessorResponseDto) mapper.entityToDto(professor, ProfessorResponseDto.class);
    }

    @Override
    public ProfessorResponseDto getProfessorByNationalId(Long professorNationalId) {
        Professor professor = professorRepository.findByProfessorNationalId(professorNationalId).get(0);
        Mapper mapper = new Mapper();
        return (ProfessorResponseDto) mapper.entityToDto(professor, ProfessorResponseDto.class);
    }

    public Professor getProfessorById(Long id) {
        Professor professor = professorRepository.findById(id).orElse(null);
        return professor;
    }

    public Professor getProfessorByNaId(Long nationalId) {
        Professor professor = professorRepository.findByProfessorNationalId(nationalId).get(0);
        if (professor.getProfessorId() > 0)
            return professor;
        else
            throw new RuntimeException("This Professor Doesn't Exist!!");
    }

    public List<ProfessorResponseDto> getProfessors() {
        List<Professor> professors = StreamSupport
                .stream((professorRepository.findAll().spliterator()), false)
                .collect(Collectors.toList());
        Mapper mapper = new Mapper();
        return mapper.entitiesToDtos(professors, ProfessorResponseDto.class);
    }

    @Override
    public List<ProfessorResponseDto> getProfessorsWithPagination(int offset, int size) {
        List<Professor> professors = professorRepository.findAll(PageRequest.of(offset, size)).getContent();
        Mapper mapper = new Mapper();
        return mapper.entitiesToDtos(professors, ProfessorResponseDto.class);
    }

    @Override
    @Transactional
    public ProfessorResponseDto addProfessor(ProfessorAddRequestDto professorAddRequestDto) {
        Professor professor = new Professor();
        for (Professor pf : professorRepository.findAll()) {
            if (pf.getProfessorNationalId().equals(professorAddRequestDto.getProfessorNationalId()) || pf.getProfessorPersonalCode() == professorAddRequestDto.getProfessorPersonalCode())
                throw new RuntimeException("This Professor With This Personal Code Or National Code Is Alredy Defined!!");
        }

        if (professorAddRequestDto.getProfessorPersonalCode() != null)
            professor.setProfessorPersonalCode(professorAddRequestDto.getProfessorPersonalCode());
        else
            throw new RuntimeException("Professor Doesn't Have Personal Code!!");

        if (professorAddRequestDto.getProfessorFirstName() != null)
            professor.setProfessorFirstName(professorAddRequestDto.getProfessorFirstName());
        else
            throw new RuntimeException("Professor Doesn't Have First Name!!");

        if (professorAddRequestDto.getProfessorLastName() != null)
            professor.setProfessorLastName(professorAddRequestDto.getProfessorLastName());
        else
            throw new RuntimeException("Professor Doesn't Have Last Name!!");

        if (professorAddRequestDto.getProfessorNationalId() != null)
            professor.setProfessorNationalId(professorAddRequestDto.getProfessorNationalId());
        else
            throw new RuntimeException("Professor Doesn't Have National Code!!");

        if (professorAddRequestDto.getPassword() != null) {
//            String hash = passwordEncoder.encode(professorAddRequestDto.getPassword());
            professor.setPassword(professorAddRequestDto.getPassword());
        } else
            throw new RuntimeException("Professor Doesn't Have Password!!");

        if (professorAddRequestDto.getDepartment() != null) {
            Department departmen = departmentServiceImp.getDepartmentById(professorAddRequestDto.getDepartment());
            professor.setDepartment(departmen);
        }

        if (professorAddRequestDto.getCourses() != null) {
            List<Course> courses = new ArrayList<>();
            for (Long courseId : professorAddRequestDto.getCourses()) {
                courses.add(courseServiceImp.getCourseById(courseId));
            }
            for (Course course : courses) {
                professor.addCourse(course);
            }
        }

        professorRepository.save(professor);

        Mapper mapper = new Mapper();
        return (ProfessorResponseDto) mapper.entityToDto(professor, ProfessorResponseDto.class);
    }

    @Override
    @Transactional
    public Professor deleteById(Long id) {
        Professor professor = professorRepository.findById(id).orElse(null);
        if (professor != null) {
            List<Course> coursesCopy = new ArrayList<>(professor.getCourses());

            for (Course course : coursesCopy) {
                professor.removeCourse(course);
            }
            if (professor.getDepartmentHead() != null) {
                throw new RuntimeException(
                        "This Professor Is Department Head of "
                                + professor.getDepartment().getDepartmentName()
                                + " You Should First Set a New Department Head for this Department!!");
            }
            professorRepository.deleteById(id);
            return professor;
        } else
            throw new RuntimeException("This Professor Isn't Defined!!");
    }

    @Override
    @Transactional
    public Professor deleteByIdNationalId(Long professorNationalId) {
        Professor professor = getProfessorByNaId(professorNationalId);
        List<Course> coursesCopy = new ArrayList<>(professor.getCourses());

        for (Course course : coursesCopy) {
            professor.removeCourse(course);
        }
        if (professor.getDepartmentHead() != null) {
            throw new RuntimeException(
                    "This Professor Is Department Head of "
                            + professor.getDepartment().getDepartmentName()
                            + " You Should First Set a New Department Head for this Department!!");
        }
        professorRepository.delete(professor);
        return professor;
    }

    @Override
    @Transactional
    public ProfessorResponseDto updateProfessor(Long id, ProfessorUpdateRequestDto professorUpdateRequestDto) {
        Professor professor = new Professor();
        boolean checkExist = false;
        for (Professor pf : professorRepository.findAll()) {
            if (pf.getProfessorId() == id) {
                checkExist = true;
                professor = pf;
                break;
            }
        }

        if (checkExist == true) {
            if (professorUpdateRequestDto.getProfessorPersonalCode() != null)
                for (Professor professor1 : professorRepository.findAll()) {
                    if (professorUpdateRequestDto.getProfessorNationalId().equals(professor1.getProfessorNationalId())) {
                        throw new RuntimeException("This professor personal code has been defined before!!");
                    }
                }
            professor.setProfessorPersonalCode(professorUpdateRequestDto.getProfessorPersonalCode());

            if (professorUpdateRequestDto.getProfessorFirstName() != null)
                professor.setProfessorFirstName(professorUpdateRequestDto.getProfessorFirstName());

            if (professorUpdateRequestDto.getProfessorLastName() != null)
                professor.setProfessorLastName(professorUpdateRequestDto.getProfessorLastName());

            if (professorUpdateRequestDto.getProfessorNationalId() != null) {
                for (Professor professor1 : professorRepository.findAll()) {
                    if (professorUpdateRequestDto.getProfessorNationalId().equals(professor1.getProfessorNationalId())) {
                        throw new RuntimeException("This professor national id has been defined before!!");
                    }
                }
                professor.setProfessorNationalId(professorUpdateRequestDto.getProfessorNationalId());
            }

            if (professorUpdateRequestDto.getPassword() != null) {
//                String hash = passwordEncoder.encode(professorUpdateRequestDto.getPassword());
                professor.setPassword(professorUpdateRequestDto.getPassword());
            }

            if (professorUpdateRequestDto.getDepartment() != null) {
                Department departmen = departmentServiceImp.getDepartmentById(professorUpdateRequestDto.getDepartment());
                professor.setDepartment(departmen);
            }

            if (professorUpdateRequestDto.getCourses() != null) {
                List<Course> courses = new ArrayList<>();
                for (Long courseId : professorUpdateRequestDto.getCourses()) {
                    courses.add(courseServiceImp.getCourseById(courseId));
                }
                List<Course> coursesCopy = new ArrayList<>(professor.getCourses());

                for (Course course : coursesCopy) {
                    professor.removeCourse(course);
                }
                for (Course course : courses) {
                    professor.addCourse(course);
                }
            }

            professorRepository.save(professor);
            Mapper mapper = new Mapper();
            return (ProfessorResponseDto) mapper.entityToDto(professor, ProfessorResponseDto.class);
        } else
            throw new RuntimeException("Professor With This Id Doesn't Exist!!");
    }

    @Override
    @Transactional
    public ProfessorResponseDto updateProfessorByNationalId(Long professorNationalId, ProfessorUpdateRequestDto professorUpdateRequestDto) {
        Professor professor = getProfessorByNaId(professorNationalId);
        if (professor.getProfessorId() > 0) {
            boolean checkExist = false;
            for (Professor pf : professorRepository.findAll()) {
                if (pf.getProfessorNationalId().equals(professorNationalId)) {
                    checkExist = true;
                    professor = pf;
                    break;
                }
            }

            if (checkExist == true) {
                if (professorUpdateRequestDto.getProfessorPersonalCode() != null)
                    for (Professor professor1 : professorRepository.findAll()) {
                        if (professorUpdateRequestDto.getProfessorPersonalCode().equals(professor1.getProfessorPersonalCode())) {
                            throw new RuntimeException("This professor personal code has been defined before!!");
                        }
                    }
                professor.setProfessorPersonalCode(professorUpdateRequestDto.getProfessorPersonalCode());

                if (professorUpdateRequestDto.getProfessorFirstName() != null)
                    professor.setProfessorFirstName(professorUpdateRequestDto.getProfessorFirstName());

                if (professorUpdateRequestDto.getProfessorLastName() != null)
                    professor.setProfessorLastName(professorUpdateRequestDto.getProfessorLastName());

                if (professorUpdateRequestDto.getProfessorNationalId() != null) {
                    for (Professor professor1 : professorRepository.findAll()) {
                        if (professorUpdateRequestDto.getProfessorNationalId().equals(professor1.getProfessorNationalId())) {
                            throw new RuntimeException("This professor national id has been defined before!!");
                        }
                    }
                    professor.setProfessorNationalId(professorUpdateRequestDto.getProfessorNationalId());
                }

                if (professorUpdateRequestDto.getPassword() != null) {
//                    String hash = passwordEncoder.encode(professorUpdateRequestDto.getPassword());
                    professor.setPassword(professorUpdateRequestDto.getPassword());
                }

                if (professorUpdateRequestDto.getDepartment() != null) {
                    Department departmen = departmentServiceImp.getDepartmentById(professorUpdateRequestDto.getDepartment());
                    professor.setDepartment(departmen);
                }

                if (professorUpdateRequestDto.getCourses() != null) {
                    List<Course> courses = new ArrayList<>();
                    for (Long courseId : professorUpdateRequestDto.getCourses()) {
                        courses.add(courseServiceImp.getCourseById(courseId));
                    }
                    List<Course> coursesCopy = new ArrayList<>(professor.getCourses());

                    for (Course course : coursesCopy) {
                        professor.removeCourse(course);
                    }
                    for (Course course : courses) {
                        professor.addCourse(course);
                    }
                }

                professorRepository.save(professor);
                Mapper mapper = new Mapper();
                return (ProfessorResponseDto) mapper.entityToDto(professor, ProfessorResponseDto.class);
            } else
                throw new RuntimeException("Professor With This Id Doesn't Exist!!");
        } else
            throw new RuntimeException("Professor With This Id Doesn't Exist!!");

    }

    @Override
    @Transactional
    public ProfessorResponseDto chooseCourse(Long professorId, Long courseId) {
        Professor professor = getProfessorById(professorId);
        Course course = courseServiceImp.getCourseById(courseId);
        if (!professor.getDepartment().getCourses().contains(course))
            throw new RuntimeException("The Course Doesn't Belong To Professor's Department");
        else if (!professor.getCourses().contains(course)) {
            professor.addCourse(course);
            professorRepository.save(professor);
            Mapper mapper = new Mapper();
            return (ProfessorResponseDto) mapper.entityToDto(professor, ProfessorResponseDto.class);
        } else
            throw new RuntimeException("The Student Has This Course!!");
    }

    @Override
    @Transactional
    public ProfessorResponseDto chooseCourseByProfessorNationalId(Long professorNationalId, Long courseId) {
        Professor professor = getProfessorByNaId(professorNationalId);
        if (professor.getProfessorId() > 0) {
            Course course = courseServiceImp.getCourseById(courseId);
            if (!professor.getDepartment().getCourses().contains(course))
                throw new RuntimeException("The Course Doesn't Belong To Professor's Department");
            else if (!professor.getCourses().contains(course)) {
                professor.addCourse(course);
                professorRepository.save(professor);
                Mapper mapper = new Mapper();
                return (ProfessorResponseDto) mapper.entityToDto(professor, ProfessorResponseDto.class);
            } else
                throw new RuntimeException("The Student Has This Course!!");
        } else
            throw new RuntimeException("The Student Has This Course!!");
    }

    @Override
    public double courseAverage(Long professorId, Long courseId) {
        Professor professor = professorRepository.findById(professorId).orElse(null);
        Course course = courseServiceImp.getCourseById(courseId);
        if (course != null && professor.getCourses().contains(course)) {
            List<Enrollment> enrollments = course.getEnrollments();
            if (!enrollments.isEmpty()) {
                double grade = 0;
                int studentCounter = 0;
                Double average = null;
                for (Enrollment enrollment : enrollments) {
                    grade += enrollment.getGrade();
                    studentCounter++;
                }
                average = grade / studentCounter;
                return average;
            } else
                throw new RuntimeException("This Course Doesn't Have any Students!!");

        } else
            throw new RuntimeException("This Course Doesn't Exist for this Professor!!");
    }

    @Override
    public double courseAverageByProfessorNationalId(Long professorNationalId, Long courseId) {
        Professor professor = getProfessorByNaId(professorNationalId);
        if (professor.getProfessorId() > 0) {
            Course course = courseServiceImp.getCourseById(courseId);
            if (course != null && professor.getCourses().contains(course)) {
                List<Enrollment> enrollments = course.getEnrollments();
                if (!enrollments.isEmpty()) {
                    double grade = 0;
                    int studentCounter = 0;
                    Double average = null;
                    for (Enrollment enrollment : enrollments) {
                        grade += enrollment.getGrade();
                        studentCounter++;
                    }
                    average = grade / studentCounter;
                    return average;
                } else
                    throw new RuntimeException("This Course Doesn't Have any Students!!");

            } else
                throw new RuntimeException("This Course Doesn't Exist for this Professor!!");
        } else
            throw new RuntimeException("This Course Doesn't Exist for this Professor!!");

    }
}
