package com.coursemanagement.FinalProject.service;

import com.coursemanagement.FinalProject.dto.requestDto.DepartmentAddRequestDto;
import com.coursemanagement.FinalProject.dto.requestDto.DepartmentUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.department.DepartmentResponseDto;
import com.coursemanagement.FinalProject.entity.Department;
import org.springframework.data.domain.Page;

import java.util.List;

public interface DepartmentService {
    public DepartmentResponseDto getDepartment(Long id);
    public Department getDepartmentById(Long id);
    public List<DepartmentResponseDto> getDepartments();
    public List<DepartmentResponseDto> getDepartmentsWithPagination(int offset, int pageSize);
    public DepartmentResponseDto addDepartment(DepartmentAddRequestDto departmentAddRequestDto);
    public Department deleteById(Long id);
    public DepartmentResponseDto updateDepartment(Long id, DepartmentUpdateRequestDto departmentUpdateRequestDto);
    public DepartmentResponseDto chooseDepartmentHead(Long departmentId, Long professorId);
    public DepartmentResponseDto chooseProfessor(Long departmentId, Long professorId);
    public double departmentAverage(Long departmentId);

}
