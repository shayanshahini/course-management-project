package com.coursemanagement.FinalProject.service;

import com.coursemanagement.FinalProject.dto.requestDto.CourseAddRequestDto;
import com.coursemanagement.FinalProject.dto.requestDto.CourseUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.course.CourseResponseDto;
import com.coursemanagement.FinalProject.entity.*;
import com.coursemanagement.FinalProject.entity.utility.EnrollmentId;
import com.coursemanagement.FinalProject.dto.mapping.reflection.Mapper;
import com.coursemanagement.FinalProject.repository.CourseRepository;
import com.coursemanagement.FinalProject.repository.EnrollmentRepository;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CourseServiceImp implements CourseService {
    private CourseRepository courseRepository;
    private DepartmentServiceImp departmentServiceImp;
    private  ProfessorServiceImp professorServiceImp;
    private StudentServiceImp studentServiceImp;
    private EnrollmentRepository enrollmentRepository;

    @Autowired
    public CourseServiceImp(CourseRepository courseRepository,
                            @Lazy DepartmentServiceImp departmentServiceImp,
                            @Lazy ProfessorServiceImp professorServiceImp,
                            StudentServiceImp studentServiceImp,
                            EnrollmentRepository enrollmentRepository) {
        this.courseRepository = courseRepository;
        this.departmentServiceImp = departmentServiceImp;
        this.professorServiceImp = professorServiceImp;
        this.studentServiceImp = studentServiceImp;
        this.enrollmentRepository = enrollmentRepository;
    }

    @Override
    public CourseResponseDto getCourse(Long id) {
        Course course = courseRepository.findById(id).orElse(null);
        Mapper mapper = new Mapper();
        return (CourseResponseDto) mapper.entityToDto(course, CourseResponseDto.class);
    }

    @Override
    public Course getCourseById(Long id) {
        Course course = courseRepository.findById(id).orElse(null);
        return course;
    }

    @Override
    public List<CourseResponseDto> getCourses() {
        List<Course> courses = StreamSupport
                .stream((courseRepository.findAll().spliterator()), false)
                .collect(Collectors.toList());
        Mapper mapper = new Mapper();
        return mapper.entitiesToDtos(courses, CourseResponseDto.class);
    }

    @Override
    public List<CourseResponseDto> getCoursesWithPagination(int offset, int size) {
        List<Course> courses = courseRepository.findAll(PageRequest.of(offset, size)).getContent();
        Mapper mapper = new Mapper();
        return mapper.entitiesToDtos(courses, CourseResponseDto.class);
    }

    @Override
    @Transactional
    public CourseResponseDto addCourse(CourseAddRequestDto courseAddRequestDto) {
        Course course = new Course();
        for (Course cs : courseRepository.findAll()) {
            if (cs != null && cs.getCourseName().equals(courseAddRequestDto.getCourseName()))
                throw new RuntimeException("This Course With This Name Is Already Defined!!");
        }

        if (courseAddRequestDto.getCourseName() != null)
            course.setCourseName(courseAddRequestDto.getCourseName());
        else
            throw new RuntimeException("Course Doesn't Have Name!!");

        if (courseAddRequestDto.getCourseCredit() > 0 && courseAddRequestDto.getCourseCredit() < 10)
            course.setCourseCredit(courseAddRequestDto.getCourseCredit());
        else
            throw new RuntimeException("Course Doesn't Have Credit!!");

        if (courseAddRequestDto.getDepartment() != null) {
            Department department = departmentServiceImp.getDepartmentById(courseAddRequestDto.getDepartment());
            course.setDepartment(department);
        }

        if (courseAddRequestDto.getProfessor() != null) {
            Professor professor = professorServiceImp.getProfessorById(courseAddRequestDto.getProfessor());
            course.setProfessor(professor);
        }

        if (courseAddRequestDto.getStudentWithGrade() != null) {
            courseAddRequestDto.getStudentWithGrade().forEach(
                    (k, v) -> {
                        Student student = studentServiceImp.getStudentById(k);
                        course.addEnrollment(
                                new Enrollment(
                                        new EnrollmentId(student.getStudentId(), course.getCourseId()),
                                        student, course, v));
                    }
            );
        }

        courseRepository.save(course);
        Mapper mapper = new Mapper();
        return (CourseResponseDto) mapper.entityToDto(course, CourseResponseDto.class);
    }

    @Override
    @Transactional
    public Course deleteById(Long id) {
        Course course = courseRepository.findById(id).orElse(null);
        if (course != null) {
            courseRepository.deleteById(id);
            return course;
        }
        else
            throw new RuntimeException("This Course Isn't Defined!!");
    }

    @Override
    @Transactional
    public CourseResponseDto updateCourse(Long id, CourseUpdateRequestDto courseUpdateRequestDto) {
        Course course = new Course();
        boolean checkExist = false;
        for (Course cs : courseRepository.findAll()) {
            if (cs.getCourseId() == id) {
                checkExist = true;
                course = cs;
                break;
            }
        }

        if (checkExist == true) {
            if (courseUpdateRequestDto.getCourseName() != null)
                course.setCourseName(courseUpdateRequestDto.getCourseName());

            if (courseUpdateRequestDto.getCourseCredit() > 0 && courseUpdateRequestDto.getCourseCredit() < 10)
                course.setCourseCredit(courseUpdateRequestDto.getCourseCredit());

            if (courseUpdateRequestDto.getDepartment() != null) {
                Department department = departmentServiceImp.getDepartmentById(courseUpdateRequestDto.getDepartment());
                course.setDepartment(department);
            }

            if (courseUpdateRequestDto.getProfessor() != null) {
                Professor professor = professorServiceImp.getProfessorById(courseUpdateRequestDto.getProfessor());
                course.setProfessor(professor);
            }

            if (courseUpdateRequestDto.getStudentWithGrade() != null) {
                List<Enrollment> enrollments = new ArrayList<>(course.getEnrollments());
                List<Enrollment> oldStudent = new ArrayList<>();
                for (Enrollment enrollment : enrollments) {
                    courseUpdateRequestDto.getStudentWithGrade().forEach(
                            (student, grade) -> {
                                if (enrollment.getStudent().getStudentId() == student && !enrollment.getGrade().equals(grade)) {
                                    enrollment.setGrade(grade);
                                    enrollmentRepository.save(enrollment);
                                    oldStudent.add(enrollment);
                                } else
                                if (enrollment.getStudent().getStudentId() == student && enrollment.getGrade().equals(grade)) {
                                    oldStudent.add(enrollment);
                                }
                            }
                    );
                    if (!oldStudent.contains(enrollment)) {
                        course.removeEnrollment(enrollment);
                        enrollmentRepository.delete(enrollment);
                    }
                }
                courseRepository.save(course);
                Course finalCourse = course;
                courseUpdateRequestDto.getStudentWithGrade().forEach(
                        (k, v) -> {
                            Student student = studentServiceImp.getStudentById(k);
                            finalCourse.addEnrollment(
                                    new Enrollment(
                                            new EnrollmentId(student.getStudentId(), finalCourse.getCourseId()),
                                            student, finalCourse, v));
                        }
                );
                course = finalCourse;
            }
            courseRepository.save(course);
            Mapper mapper = new Mapper();
            return (CourseResponseDto) mapper.entityToDto(course, CourseResponseDto.class);
        } else
            throw new RuntimeException("Course With This Id Doesn't Exist!!");
    }

    @Override
    @Transactional
    public CourseResponseDto chooseStudent(Long courseId, Long studentId, Double grade) {
        Course course = getCourseById(courseId);
        Student student = studentServiceImp.getStudentById(studentId);
        List<Enrollment> enrollments = course.getEnrollments();
        EnrollmentId enrollmentId = new EnrollmentId();
        enrollmentId.setStudentId(studentId);
        enrollmentId.setCourseId(courseId);
        boolean check = false;
        for (Enrollment enrollment : enrollments) {
            if (enrollment.getEnrollmentId().equals(enrollmentId)) {
                check = true;
            }
        }
        if (check == false) {
            course.addEnrollment(new Enrollment(
                    new EnrollmentId(student.getStudentId(), course.getCourseId()), student, course, grade));
            courseRepository.save(course);
            Mapper mapper = new Mapper();
            return (CourseResponseDto) mapper.entityToDto(course, CourseResponseDto.class);
        }

        else
            throw new RuntimeException("The Student Has This Course!!");
    }
}

