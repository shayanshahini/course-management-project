package com.coursemanagement.FinalProject.service;

import com.coursemanagement.FinalProject.dto.requestDto.DepartmentAddRequestDto;
import com.coursemanagement.FinalProject.dto.requestDto.DepartmentUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.department.DepartmentResponseDto;
import com.coursemanagement.FinalProject.entity.*;
import com.coursemanagement.FinalProject.dto.mapping.reflection.Mapper;
import com.coursemanagement.FinalProject.repository.DepartmentRepository;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class DepartmentServiceImp implements DepartmentService {
    private DepartmentRepository departmentRepository;
    private ProfessorServiceImp professorServiceImp;
    private StudentServiceImp studentServiceImp;
    private CourseServiceImp courseServiceImp;

    @Autowired
    public DepartmentServiceImp(DepartmentRepository departmentRepository,
                                ProfessorServiceImp professorServiceImp,
                                StudentServiceImp studentServiceImp,
                                CourseServiceImp courseServiceImp) {
        this.departmentRepository = departmentRepository;
        this.professorServiceImp = professorServiceImp;
        this.studentServiceImp = studentServiceImp;
        this.courseServiceImp = courseServiceImp;
    }

    @Override
    public DepartmentResponseDto getDepartment(Long id) {
        Department department = departmentRepository.findById(id).orElse(null);
        Mapper mapper = new Mapper();
        return (DepartmentResponseDto) mapper.entityToDto(department, DepartmentResponseDto.class);
    }

    @Override
    public Department getDepartmentById(Long id) {
        Department department = departmentRepository.findById(id).orElse(null);
        if (department != null) {
            return department;
        } else
            throw new RuntimeException("Department With This Id Doesn't Excite");
    }

    @Override
    public List<DepartmentResponseDto> getDepartments() {
        List<Department> departments = StreamSupport
                .stream((departmentRepository.findAll().spliterator()), false)
                .collect(Collectors.toList());
        Mapper mapper = new Mapper();
        return mapper.entitiesToDtos(departments, DepartmentResponseDto.class);
    }

    @Override
    public List<DepartmentResponseDto> getDepartmentsWithPagination(int offset, int size) {
        List<Department> departments = departmentRepository.findAll(PageRequest.of(offset, size)).getContent();
        Mapper mapper = new Mapper();
        return mapper.entitiesToDtos(departments, DepartmentResponseDto.class);
    }

    @Override
    @Transactional
    public DepartmentResponseDto addDepartment(DepartmentAddRequestDto departmentAddRequestDto) {
        Department department = new Department();

        for (Department dp : departmentRepository.findAll()) {
            if (dp.getDepartmentName().equals(departmentAddRequestDto.getDepartmentName()))
                throw new RuntimeException("This Department Is Already Defined!!");
        }

        if (departmentAddRequestDto.getDepartmentName() != null) {
            department.setDepartmentName(departmentAddRequestDto.getDepartmentName());
        } else
            throw new RuntimeException("Department Doesn't Have Name!!");

        if (departmentAddRequestDto.getProfessors() != null) {
            List<Long> professorsId = departmentAddRequestDto.getProfessors();
            List<Professor> professors = new ArrayList<>();
            for (Long professorId : professorsId) {
                professors.add(professorServiceImp.getProfessorById(professorId));
            }
            for (Professor professor : professors) {
                department.addProfessor(professor);
            }
        }

        Professor departmentHead;
        if (departmentAddRequestDto.getDepartmentHead() != null) {
            departmentHead = professorServiceImp.getProfessorById(departmentAddRequestDto.getDepartmentHead());
            department.setDepartmentHead(departmentHead);
        } else
            throw new RuntimeException("Department Doesn't Have Department Head!!");

        if (department.getProfessors().contains(departmentHead))
            department.setDepartmentHead(departmentHead);
        else
            throw new RuntimeException("Department Head Isn't Member Of This Department");

        List<Long> studentsId;
        if (departmentAddRequestDto.getStudents() != null) {
            studentsId = departmentAddRequestDto.getStudents();
            List<Student> students = new ArrayList<>();
            for (Long studentId : studentsId) {
                students.add(studentServiceImp.getStudentById(studentId));
            }
            for (Student student : students) {
                department.addStudent(student);
            }
        }

        if (departmentAddRequestDto.getCourses() != null) {
            List<Long> coursesId = departmentAddRequestDto.getCourses();
            List<Course> courses = new ArrayList<>();
            for (Long courseId : coursesId) {
                courses.add(courseServiceImp.getCourseById(courseId));
            }
            for (Course course : courses) {
                department.addCourse(course);
            }
        } else
            throw new RuntimeException("Department Doesn't Have Any Course");

        departmentRepository.save(department);
        Mapper mapper = new Mapper();
        return (DepartmentResponseDto) mapper.entityToDto(department, DepartmentResponseDto.class);
    }

    @Override
    @Transactional
    public Department deleteById(Long id) {
        Department department = departmentRepository.findById(id).orElse(null);
        if (department != null) {
            List<Course> coursesCopy = new ArrayList<>(department.getCourses());
            List<Student> studentsCopy = new ArrayList<>(department.getStudents());
            List<Professor> professorsCopy = new ArrayList<>(department.getProfessors());

            for (Course course : coursesCopy) {
                department.removeCourse(course);
            }
            for (Student student : studentsCopy) {
                department.removeStudent(student);
            }
            for (Professor professor : professorsCopy) {
                department.removeProfessor(professor);
            }

            Professor headProfessor = department.getDepartmentHead();
            headProfessor.setDepartmentHead(null);
            departmentRepository.deleteById(id);
            return department;
        } else
            throw new RuntimeException("This Department Isn't Defined!!");
    }

    @Override
    @Transactional
    public DepartmentResponseDto updateDepartment(Long id, DepartmentUpdateRequestDto departmentUpdateRequestDto) {
        Department department = new Department();
        boolean checkExist = false;
        for (Department dp : departmentRepository.findAll()) {
            if (dp.getDepartmentId() == id) {
                checkExist = true;
                department = dp;
                break;
            }
        }

        if (checkExist == true) {
            if (departmentUpdateRequestDto.getDepartmentName() != null) {
                department.setDepartmentName(departmentUpdateRequestDto.getDepartmentName());
            }
            if (departmentUpdateRequestDto.getProfessors() != null) {
                List<Long> professorsId = departmentUpdateRequestDto.getProfessors();
                List<Professor> professors = new ArrayList<>();
                for (Long professorId : professorsId) {
                    professors.add(professorServiceImp.getProfessorById(professorId));
                }
                List<Professor> professorCopy = new ArrayList<>(department.getProfessors());

                for (Professor professor : professorCopy) {
                    department.removeProfessor(professor);
                }
                for (Professor professor : professors) {
                    department.addProfessor(professor);
                }
            }

            Professor departmentHead = new Professor();
            if (departmentUpdateRequestDto.getDepartmentHead() != null) {
                departmentHead = professorServiceImp.getProfessorById(departmentUpdateRequestDto.getDepartmentHead());
            }
            if (department.getProfessors().contains(departmentHead))
                department.setDepartmentHead(departmentHead);
            else if (departmentUpdateRequestDto.getDepartmentHead() != null)
                throw new RuntimeException("Department Head Isn't Member Of This Department");

            List<Long> studentsId;
            if (departmentUpdateRequestDto.getStudents() != null) {
                studentsId = departmentUpdateRequestDto.getStudents();
                List<Student> students = new ArrayList<>();
                for (Long studentId : studentsId) {
                    students.add(studentServiceImp.getStudentById(studentId));
                }
                List<Student> studentsCopy = new ArrayList<>(department.getStudents());

                for (Student student : studentsCopy) {
                    department.removeStudent(student);
                }
                for (Student student : students) {
                    department.addStudent(student);
                }
            }

            if (departmentUpdateRequestDto.getCourses() != null) {
                List<Long> coursesId = departmentUpdateRequestDto.getCourses();
                List<Course> courses = new ArrayList<>();
                for (Long courseId : coursesId) {
                    courses.add(courseServiceImp.getCourseById(courseId));
                }
                List<Course> coursesCopy = new ArrayList<>(department.getCourses());

                for (Course course : coursesCopy) {
                    department.removeCourse(course);
                }
                for (Course course : courses) {
                    department.addCourse(course);
                }
            }
            departmentRepository.save(department);
            Mapper mapper = new Mapper();
            return (DepartmentResponseDto) mapper.entityToDto(department, DepartmentResponseDto.class);
        } else
            throw new RuntimeException("Department With This Id Doesn't Exist!!");
    }

    @Override
    @Transactional
    public DepartmentResponseDto chooseDepartmentHead(Long departmentId, Long professorId) {
        Department department = departmentRepository.findById(departmentId).orElse(null);
        for (Department department1 : departmentRepository.findAll()) {
            if (department1.getDepartmentHead() != null && department1.getDepartmentHead().equals(professorServiceImp.getProfessorById(professorId))) {
                throw new RuntimeException(
                        "This Professor Is Already The Department Head Of Department: "
                                + department1.getDepartmentName());
            }
        }
        if (department.getProfessors().contains(professorServiceImp.getProfessorById(professorId))) {
            department.setDepartmentHead(professorServiceImp.getProfessorById(professorId));
            departmentRepository.save(department);
            Mapper mapper = new Mapper();
            return (DepartmentResponseDto) mapper.entityToDto(department, DepartmentResponseDto.class);
        } else
            throw new RuntimeException("This Professor Isn't Member Of This Department");
    }

    @Override
    @Transactional
    public DepartmentResponseDto chooseProfessor(Long departmentId, Long professorId) {
        Department department = getDepartmentById(departmentId);
        Professor professor = professorServiceImp.getProfessorById(professorId);
        department.addProfessor(professor);
        departmentRepository.save(department);
        Mapper mapper = new Mapper();
        return (DepartmentResponseDto) mapper.entityToDto(department, DepartmentResponseDto.class);
    }

    @Override
    public double departmentAverage(Long departmentId) {
        Department department = departmentRepository.findById(departmentId).orElse(null);
        List<Student> students = department.getStudents();
        Double departmentAverage = null;
        if (!students.isEmpty()) {
            double totalStudentsAverage = 0;
            int studentCounter = 0;
            for (Student student : students) {
                List<Enrollment> enrollments = student.getEnrollments();
                double totalSum = 0;
                int courseCounter = 0;
                if (!enrollments.isEmpty()) {
                    for (Enrollment enrollment : enrollments) {
                        totalSum += enrollment.getGrade();
                        courseCounter++;
                    }
                    totalStudentsAverage += totalSum / (double) courseCounter;
                    studentCounter++;
                } else
                    throw new RuntimeException("This student doesn't have any courses!!");
                departmentAverage = totalStudentsAverage / studentCounter;
            }
            return departmentAverage;
        } else
            throw new RuntimeException("This department doesn't have any students!!");
    }
}
