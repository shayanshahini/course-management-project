package com.coursemanagement.FinalProject.dto.mapping.reflection;

import com.coursemanagement.FinalProject.entity.Course;
import com.coursemanagement.FinalProject.entity.Enrollment;
import com.coursemanagement.FinalProject.entity.Student;
import org.hibernate.Hibernate;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;

public class Mapper<E, D> {

    public D entityToDto(E entity, Class<D> dtoClass) {
        try {
            D dto = dtoClass.getDeclaredConstructor().newInstance();

            E unwrappedEntity = (E) Hibernate.unproxy(entity);
            Field[] entityFields = unwrappedEntity.getClass().getDeclaredFields();
            Field[] dtoFields = dtoClass.getDeclaredFields();

            for (Field dtoField : dtoFields) {
                for (Field entityField : entityFields) {
                    entityField.setAccessible(true);
                    dtoField.setAccessible(true);

                    if (dtoField.getName().equals(entityField.getName()) && entityField.get(unwrappedEntity) != null || entityField.getName().equals("enrollments")) {
                        if (dtoField.getType().isAssignableFrom(List.class) && entityField.getType().isAssignableFrom(List.class)) {
                            Type genericType = dtoField.getGenericType();
                            if (genericType instanceof ParameterizedType) {
                                Type elementType = ((ParameterizedType) genericType).getActualTypeArguments()[0];
                                Class<?> elementClass = (Class<?>) elementType;

                                List<?> entityList = (List<?>) entityField.get(unwrappedEntity);
                                if (entityList != null) {
                                    List<Object> dtoList = entityList.stream()
                                            .map(entityItem -> {
                                                if (entityItem instanceof Enrollment) {
                                                    if (unwrappedEntity.getClass().getSimpleName().equals("Student")) {
                                                        Course course = ((Enrollment) entityItem).getCourse();
                                                        Mapper<Object, Object> nestedMapper = new Mapper<>();
                                                        return nestedMapper.entityToDto(course, (Class) elementClass);
                                                    } else if (unwrappedEntity.getClass().getSimpleName().equals("Course")) {
                                                        Student student = ((Enrollment) entityItem).getStudent();
                                                        Mapper<Object, Object> nestedMapper = new Mapper<>();
                                                        return nestedMapper.entityToDto(student, (Class) elementClass);
                                                    }

                                                }
                                                else {
                                                    Mapper<Object, Object> nestedMapper = new Mapper<>();
                                                    return nestedMapper.entityToDto(entityItem, (Class) elementClass);
                                                }
                                                return null;
                                            })
                                            .collect(Collectors.toList());

                                    dtoField.set(dto, dtoList);
                                }
                            }
                        } else if (!dtoField.getType().getSimpleName().equals(entityField.getType().getSimpleName())) {
                            Mapper<Object, Object> nestedMapper = new Mapper<>();
                            dtoField.set(dto, nestedMapper.entityToDto(entityField.get(unwrappedEntity), (Class) dtoField.getType()));
                        } else {
                            dtoField.set(dto, entityField.get(unwrappedEntity));
                        }
                        break;
                    }
                }
            }

            return dto;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error mapping entity to DTO", e);
        }
    }

    public List<D> entitiesToDtos(List<E> entities, Class<D> dtoClass) {
        return entities.stream()
                .map(entity -> entityToDto(entity, dtoClass))
                .collect(Collectors.toList());
    }
}

