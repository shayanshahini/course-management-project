package com.coursemanagement.FinalProject.dto.mapping.deprecated;

import com.coursemanagement.FinalProject.dto.responseDto.course.CourseResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.course.DepartmentInCourseResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.course.StudentInCourseResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.department.ProfessorInDepartmentResponseDto;
import com.coursemanagement.FinalProject.entity.Course;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
@Deprecated
public class CourseEntityToDto {
    public static CourseResponseDto courseToDto(Course course) {

        CourseResponseDto courseResponseDto = new CourseResponseDto();

        if (course.getCourseName() != null)
            courseResponseDto.setCourseName(course.getCourseName());
        else
            throw new RuntimeException("Course Doesn't Have Name!!");

        if (course.getCourseCredit() < 10 && course.getCourseCredit() > 0)
            courseResponseDto.setCourseCredit(course.getCourseCredit());
        else
            throw new RuntimeException("Course Doesn't Have Valid Credit");


        DepartmentInCourseResponseDto departmentInCourseResponseDto = new DepartmentInCourseResponseDto();
        if (course.getDepartment() != null) {
            departmentInCourseResponseDto.setDepartmentName(course.getDepartment().getDepartmentName());
            ProfessorInDepartmentResponseDto headInDepartmentResponseDto = new ProfessorInDepartmentResponseDto();
            headInDepartmentResponseDto.setProfessorPersonalCode(course.getDepartment().getDepartmentHead().getProfessorPersonalCode());
            headInDepartmentResponseDto.setProfessorFirstName(course.getDepartment().getDepartmentHead().getProfessorFirstName());
            headInDepartmentResponseDto.setProfessorLastName(course.getDepartment().getDepartmentHead().getProfessorLastName());
            headInDepartmentResponseDto.setProfessorNationalId(course.getDepartment().getDepartmentHead().getProfessorNationalId());
            departmentInCourseResponseDto.setDepartmentHead(headInDepartmentResponseDto);
        }

        if ((course.getDepartment() != null)) {
            List<ProfessorInDepartmentResponseDto> professorInDepartmentResponseDtos = course.getDepartment().getProfessors().stream()
                    .map(professor -> new ProfessorInDepartmentResponseDto(
                            professor.getProfessorPersonalCode(),
                            professor.getProfessorFirstName(),
                            professor.getProfessorLastName(),
                            professor.getProfessorNationalId()))
                    .collect(Collectors.toList());
            departmentInCourseResponseDto.setProfessors(professorInDepartmentResponseDtos);

            courseResponseDto.setDepartment(departmentInCourseResponseDto);
        }


        if(course.getProfessor() != null) {
            ProfessorInDepartmentResponseDto professorInDepartmentResponseDto = new ProfessorInDepartmentResponseDto();
            professorInDepartmentResponseDto.setProfessorPersonalCode(course.getProfessor().getProfessorPersonalCode());
            professorInDepartmentResponseDto.setProfessorFirstName(course.getProfessor().getProfessorFirstName());
            professorInDepartmentResponseDto.setProfessorLastName(course.getProfessor().getProfessorLastName());
            professorInDepartmentResponseDto.setProfessorNationalId(course.getProfessor().getProfessorNationalId());
            courseResponseDto.setProfessor(professorInDepartmentResponseDto);
        }

        if (course.getEnrollments() != null) {
            List<StudentInCourseResponseDto> studentInCourseResponseDtos = course.getEnrollments().stream()
                    .map(enrollment -> new StudentInCourseResponseDto(
                            enrollment.getStudent().getStudentUniversityId(),
                            enrollment.getStudent().getStudentFirstName(),
                            enrollment.getStudent().getStudentLastName(),
                            enrollment.getStudent().getStudentNationalId(),
                            enrollment.getStudent().getAddress()))
                    .collect(Collectors.toList());
            courseResponseDto.setStudents(studentInCourseResponseDtos);
        }

        return courseResponseDto;
    }

    public static List<CourseResponseDto> coursetoDtos(List<Course> courses) {
        List<CourseResponseDto> courseResponseDtos = new ArrayList<>();
        for (Course course : courses) {
            courseResponseDtos.add(courseToDto(course));
        }
        return courseResponseDtos;
    }
    }

