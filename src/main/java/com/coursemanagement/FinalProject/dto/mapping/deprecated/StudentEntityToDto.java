package com.coursemanagement.FinalProject.dto.mapping.deprecated;

import com.coursemanagement.FinalProject.dto.responseDto.student.*;
import com.coursemanagement.FinalProject.entity.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
@Deprecated
public class StudentEntityToDto {
    public static StudentResponseDto studentToDto(Student student) {
        StudentResponseDto studentResponseDto = new StudentResponseDto();

        if (student.getStudentUniversityId() != null)
            studentResponseDto.setStudentUniversityId(student.getStudentUniversityId());
        else
            throw new RuntimeException("Student Doesn't Have University Id!!");

        if (student.getStudentFirstName() != null)
            studentResponseDto.setStudentFirstName(student.getStudentFirstName());
        else
            throw new RuntimeException("Student Doesn't Have First Name!!");

        if (student.getStudentLastName() != null)
            studentResponseDto.setStudentLastName(student.getStudentLastName());
        else
            throw new RuntimeException("Student Doesn't Have Last Name!!");

        if (student.getStudentNationalId() != null)
            studentResponseDto.setStudentNationalId(student.getStudentNationalId());
        else
            throw new RuntimeException("Student Doesn't Have National Id!!");

        if (student.getPassword() != null) {
            studentResponseDto.setPassword(student.getPassword());
        }
        else
            throw new RuntimeException("Student Doesn't Have Password!!");

        if (student.getAddress() != null)
            studentResponseDto.setAddress(student.getAddress());

        DepartmentInStudentResponseDto departmentInStudentResponseDto = new DepartmentInStudentResponseDto();
        if (student.getDepartment() != null)
            departmentInStudentResponseDto.setDepartmentName(student.getDepartment().getDepartmentName());
        ProfessorInStudentResponseDto headInStudentResponseDto = new ProfessorInStudentResponseDto();
        if (student.getDepartment() != null && student.getDepartment().getDepartmentHead() != null)
        {
            headInStudentResponseDto.setProfessorPersonalCode(student.getDepartment().getDepartmentHead().getProfessorPersonalCode());
            headInStudentResponseDto.setProfessorFirstName(student.getDepartment().getDepartmentHead().getProfessorFirstName());
            headInStudentResponseDto.setProfessorLastName(student.getDepartment().getDepartmentHead().getProfessorLastName());
            headInStudentResponseDto.setProfessorNationalId(student.getDepartment().getDepartmentHead().getProfessorNationalId());
            departmentInStudentResponseDto.setDepartmentHead(headInStudentResponseDto);
        }

        if (student.getDepartment() != null) {
            List<ProfessorInStudentResponseDto> professorInStudentResponseDtos = student.getDepartment().getProfessors().stream()
                    .map(professor -> new ProfessorInStudentResponseDto(
                            professor.getProfessorPersonalCode(),
                            professor.getProfessorFirstName(),
                            professor.getProfessorLastName(),
                            professor.getProfessorNationalId()))
                    .collect(Collectors.toList());
            departmentInStudentResponseDto.setProfessors(professorInStudentResponseDtos);
            studentResponseDto.setDepartment(departmentInStudentResponseDto);
        }

        if (student.getEnrollments() != null) {
            List<TempCourseInStudentResponseDto> tempCourseInStudentResponseDtos = student.getEnrollments().stream()
                    .map(enrollment -> new TempCourseInStudentResponseDto(
                            enrollment.getCourse().getCourseName(),
                            enrollment.getCourse().getCourseCredit(),
                            enrollment.getCourse().getProfessor()))
                    .collect(Collectors.toList());

            List<CourseInStudentResponseDto> courseInStudentResponseDtos = tempCourseInStudentResponseDtos.stream()
                    .map(course -> {
                        if (course.getProfessor() == null) {
                            return new CourseInStudentResponseDto(
                                    course.getCourseName(),
                                    course.getCourseCredit(),
                                    null
                            );
                        } else {
                            return new CourseInStudentResponseDto(
                                    course.getCourseName(),
                                    course.getCourseCredit(),
                                    new ProfessorInStudentResponseDto(
                                            course.getProfessor().getProfessorPersonalCode(),
                                            course.getProfessor().getProfessorFirstName(),
                                            course.getProfessor().getProfessorLastName(),
                                            course.getProfessor().getProfessorNationalId()
                                    )
                            );
                        }
                    })
                    .collect(Collectors.toList());

            studentResponseDto.setCourses(courseInStudentResponseDtos);
        } else
            throw new RuntimeException("Student Doesn't Have Any Course!!");

        return studentResponseDto;
    }

    public static List<StudentResponseDto> studentResponseDtos(List<Student> students) {
        List<StudentResponseDto> studentResponseDtos = new ArrayList<>();
        for (Student student : students) {
            studentResponseDtos.add(studentToDto(student));
        }
        return studentResponseDtos;
    }
}

