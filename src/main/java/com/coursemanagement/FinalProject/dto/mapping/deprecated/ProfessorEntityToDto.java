package com.coursemanagement.FinalProject.dto.mapping.deprecated;

import com.coursemanagement.FinalProject.dto.responseDto.department.ProfessorInDepartmentResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.professor.CourseInProfessorResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.professor.DepartmentInProfessorResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.professor.ProfessorResponseDto;
import com.coursemanagement.FinalProject.entity.Professor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
@Deprecated
public class ProfessorEntityToDto {
    public static ProfessorResponseDto professorToDto(Professor professor) {
        ProfessorResponseDto professorResponseDto = new ProfessorResponseDto();

        if (professor.getProfessorPersonalCode() != null)
            professorResponseDto.setProfessorPersonalCode(professor.getProfessorPersonalCode());
        else
            throw new RuntimeException("Professor Doesn't Have Personal Code!!");

        if (professor.getProfessorFirstName() != null)
            professorResponseDto.setProfessorFirstName(professor.getProfessorFirstName());
        else
            throw new RuntimeException("Professor Doesn't Have First Name!!");

        if (professor.getProfessorLastName() != null)
            professorResponseDto.setProfessorLastName(professor.getProfessorLastName());
        else
            throw new RuntimeException("Professor Doesn't Have Last Name!!");

        if (professor.getProfessorNationalId() != null)
            professorResponseDto.setProfessorNationalId(professor.getProfessorNationalId());
        else
            throw new RuntimeException("Professor Doesn't Have National Id!!");

        DepartmentInProfessorResponseDto departmentInProfessorResponseDto = new DepartmentInProfessorResponseDto();
        if (professor.getDepartment() != null && professor.getDepartment().getDepartmentHead() != null) {
            departmentInProfessorResponseDto.setDepartmentName(professor.getDepartment().getDepartmentName());
            ProfessorInDepartmentResponseDto headInDepartmentResponseDto = new ProfessorInDepartmentResponseDto();
            headInDepartmentResponseDto.setProfessorPersonalCode(professor.getDepartment().getDepartmentHead().getProfessorPersonalCode());
            headInDepartmentResponseDto.setProfessorFirstName(professor.getDepartment().getDepartmentHead().getProfessorFirstName());
            headInDepartmentResponseDto.setProfessorLastName(professor.getDepartment().getDepartmentHead().getProfessorLastName());
            headInDepartmentResponseDto.setProfessorNationalId(professor.getDepartment().getDepartmentHead().getProfessorNationalId());
            departmentInProfessorResponseDto.setDepartmentHead(headInDepartmentResponseDto);
        }

        if (professor.getDepartment() != null) {
            List<ProfessorInDepartmentResponseDto> professorInDepartmentResponseDtos = professor.getDepartment().getProfessors().stream()
                    .map(departmentProfessor -> new ProfessorInDepartmentResponseDto(
                            departmentProfessor.getProfessorPersonalCode(),
                            departmentProfessor.getProfessorFirstName(),
                            departmentProfessor.getProfessorLastName(),
                            departmentProfessor.getProfessorNationalId()))
                    .collect(Collectors.toList());
//            departmentInProfessorResponseDto.setProfessors(professorInDepartmentResponseDtos);

            professorResponseDto.setDepartment(departmentInProfessorResponseDto);
        }

        if (professor.getCourses() != null) {
            List<CourseInProfessorResponseDto> courseInProfessorResponseDtos = professor.getCourses().stream()
                    .map(course -> new CourseInProfessorResponseDto(
                            course.getCourseName(),
                            course.getCourseCredit()))
                    .collect(Collectors.toList());
//            professorResponseDto.setCourses(courseInProfessorResponseDtos);
        }

        return professorResponseDto;
    }

    public static List<ProfessorResponseDto> professorToDtos(List<Professor> professors) {
        List<ProfessorResponseDto> professorResponseDtos = new ArrayList<>();
        for (Professor professor : professors) {
            professorResponseDtos.add(professorToDto(professor));
        }
        return professorResponseDtos;
    }
}

