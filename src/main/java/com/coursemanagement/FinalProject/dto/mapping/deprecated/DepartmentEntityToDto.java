package com.coursemanagement.FinalProject.dto.mapping.deprecated;

import com.coursemanagement.FinalProject.dto.responseDto.department.CourseInDepartmentResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.department.DepartmentResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.department.ProfessorInDepartmentResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.department.StudentInDepartmentResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.department.CourseInStudentInDepartmentResponseDto;
import com.coursemanagement.FinalProject.entity.Department;
import com.coursemanagement.FinalProject.entity.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
@Deprecated
public class DepartmentEntityToDto {
    public static DepartmentResponseDto departmentToDto(Department department) {

        DepartmentResponseDto departmentResponseDto = new DepartmentResponseDto();

        //**********SET NAME**********//
        if (department.getDepartmentName() != null)
            departmentResponseDto.setDepartmentName(department.getDepartmentName());
        else
            throw new RuntimeException("Department Doesn't Have Name!!");

        //**********SET HEAD**********//
        if (department.getDepartmentHead() != null || department.getProfessors() != null) {
            ProfessorInDepartmentResponseDto headInDepartmentResponseDto = new ProfessorInDepartmentResponseDto();
            headInDepartmentResponseDto.setProfessorPersonalCode(department.getDepartmentHead().getProfessorPersonalCode());
            headInDepartmentResponseDto.setProfessorFirstName(department.getDepartmentHead().getProfessorFirstName());
            headInDepartmentResponseDto.setProfessorLastName(department.getDepartmentHead().getProfessorLastName());
            headInDepartmentResponseDto.setProfessorNationalId(department.getDepartmentHead().getProfessorNationalId());
            departmentResponseDto.setDepartmentHead(headInDepartmentResponseDto);
        }
        else
            throw new RuntimeException("Department Doesn't Have Head!!");

        //**********SET PROFESSOR**********//
        if (department.getProfessors() != null) {
            List<ProfessorInDepartmentResponseDto> professorInDepartmentResponseDtos = department.getProfessors().stream()
                    .map(professor -> new ProfessorInDepartmentResponseDto(
                            professor.getProfessorPersonalCode(),
                            professor.getProfessorFirstName(),
                            professor.getProfessorLastName(),
                            professor.getProfessorNationalId()))
                    .collect(Collectors.toList());
            departmentResponseDto.setProfessors(professorInDepartmentResponseDtos);
        }

        //**********SET STUDENT**********//
        if (department.getStudents() != null) {
            List<StudentInDepartmentResponseDto> studentInDepartmentResponseDtos = new ArrayList<>();
            for (Student student : department.getStudents()) {
                StudentInDepartmentResponseDto studentInDepartmentResponseDto = new StudentInDepartmentResponseDto();
                studentInDepartmentResponseDto.setStudentUniversityId(student.getStudentUniversityId());
                studentInDepartmentResponseDto.setStudentFirstName(student.getStudentFirstName());
                studentInDepartmentResponseDto.setStudentLastName(student.getStudentLastName());
                studentInDepartmentResponseDto.setStudentNationalId(student.getStudentNationalId());
                if (student.getAddress() != null)
                    studentInDepartmentResponseDto.setAddress(student.getAddress());

                if (student.getEnrollments() != null) {
                    List<CourseInStudentInDepartmentResponseDto> courseInStudentInDepartmentResponseDtos = student.getEnrollments().stream()
                            .map(enrollment -> new CourseInStudentInDepartmentResponseDto(
                                    enrollment.getCourse().getCourseName(),
                                    enrollment.getCourse().getCourseCredit()))
                            .collect(Collectors.toList());
                    studentInDepartmentResponseDto.setCourses(courseInStudentInDepartmentResponseDtos);
                }
                else
                    throw new RuntimeException("Student Doesn't Have Any Course!!");

                studentInDepartmentResponseDtos.add(studentInDepartmentResponseDto);
            }
            departmentResponseDto.setStudents(studentInDepartmentResponseDtos);
        }

        //**********SET COURSE**********//
        if (department.getCourses() != null) {
            List<CourseInDepartmentResponseDto> courseInDepartmentResponseDtos = department.getCourses().stream()
                    .map(course -> new CourseInDepartmentResponseDto(
                            course.getCourseName(),
                            course.getCourseCredit()))
                    .collect(Collectors.toList());
            departmentResponseDto.setCourses(courseInDepartmentResponseDtos);
        }
        else
            throw new RuntimeException("Department Doesn't Have Any Course!!");
        //**********RETURN DTO**********//
        return departmentResponseDto;
    }

    public static List<DepartmentResponseDto> departmentToDtos(List<Department> departments) {

        List<DepartmentResponseDto> departmentResponseDtos = new ArrayList<>();
        for (Department department : departments) {
            departmentResponseDtos.add(departmentToDto(department));
        }
        return departmentResponseDtos;
    }
    }

