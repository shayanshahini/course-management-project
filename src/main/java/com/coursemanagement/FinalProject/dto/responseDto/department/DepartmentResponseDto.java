package com.coursemanagement.FinalProject.dto.responseDto.department;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentResponseDto {
    private String departmentName;
    private ProfessorInDepartmentResponseDto departmentHead;
    private List<ProfessorInDepartmentResponseDto> professors;
    private List<StudentInDepartmentResponseDto> students;
    private List<CourseInDepartmentResponseDto> courses;
}
