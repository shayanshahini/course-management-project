package com.coursemanagement.FinalProject.dto.responseDto.department;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseInStudentInDepartmentResponseDto {
    private String courseName;
    private int courseCredit;
}
