package com.coursemanagement.FinalProject.dto.responseDto.course;

import com.coursemanagement.FinalProject.dto.responseDto.department.ProfessorInDepartmentResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.professor.DepartmentInProfessorResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.student.StudentResponseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseResponseDto {
    private List<StudentInCourseResponseDto> students;
    private String courseName;
    private int courseCredit;
    private DepartmentInCourseResponseDto department;
    private ProfessorInDepartmentResponseDto professor;
}
