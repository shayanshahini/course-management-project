package com.coursemanagement.FinalProject.dto.responseDto.professor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseInProfessorResponseDto {
    private String courseName;
    private int courseCredit;
}
