package com.coursemanagement.FinalProject.dto.responseDto.student;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfessorInStudentResponseDto {
    private Long professorPersonalCode;
    private String professorFirstName;
    private String professorLastName;
    private Long professorNationalId;
}
