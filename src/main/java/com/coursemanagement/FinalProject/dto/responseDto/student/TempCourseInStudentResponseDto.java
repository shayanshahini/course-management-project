package com.coursemanagement.FinalProject.dto.responseDto.student;

import com.coursemanagement.FinalProject.entity.Professor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TempCourseInStudentResponseDto {
    private String courseName;
    private int courseCredit;
    private Professor professor;
}
