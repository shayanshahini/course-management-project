package com.coursemanagement.FinalProject.dto.responseDto.course;

import com.coursemanagement.FinalProject.dto.responseDto.department.ProfessorInDepartmentResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.professor.DepartmentInProfessorResponseDto;
import com.coursemanagement.FinalProject.dto.responseDto.student.StudentResponseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentInCourseResponseDto {
    private Long studentUniversityId;
    private String studentFirstName;
    private String studentLastName;
    private Long studentNationalId;
    private String address;
}
