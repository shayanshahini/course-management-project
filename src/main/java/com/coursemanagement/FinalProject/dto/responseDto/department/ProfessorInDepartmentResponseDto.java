package com.coursemanagement.FinalProject.dto.responseDto.department;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfessorInDepartmentResponseDto {
    private Long professorPersonalCode;
    private String professorFirstName;
    private String professorLastName;
    private Long professorNationalId;
}
