package com.coursemanagement.FinalProject.dto.responseDto.professor;

import com.coursemanagement.FinalProject.dto.responseDto.department.CourseInDepartmentResponseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfessorResponseDto {
    private List<CourseInProfessorResponseDto> courses;
    private Long professorPersonalCode;
    private String professorFirstName;
    private String professorLastName;
    private Long professorNationalId;
    private DepartmentInProfessorResponseDto department;

}
