package com.coursemanagement.FinalProject.dto.responseDto.student;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseInStudentResponseDto {
    private String courseName;
    private int courseCredit;
    private ProfessorInStudentResponseDto professor;
}
