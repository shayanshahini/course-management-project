package com.coursemanagement.FinalProject.dto.responseDto.department;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentInDepartmentResponseDto {
    private Long studentUniversityId;
    private String studentFirstName;
    private String studentLastName;
    private Long studentNationalId;
    private String address;
    private List<CourseInStudentInDepartmentResponseDto> courses;

}
