package com.coursemanagement.FinalProject.dto.responseDto.department;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseInDepartmentResponseDto {
    private String courseName;
    private int courseCredit;
}
