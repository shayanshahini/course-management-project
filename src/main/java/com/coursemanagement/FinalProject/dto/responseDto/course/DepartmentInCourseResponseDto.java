package com.coursemanagement.FinalProject.dto.responseDto.course;

import com.coursemanagement.FinalProject.dto.responseDto.department.ProfessorInDepartmentResponseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentInCourseResponseDto {
    private String departmentName;
    private ProfessorInDepartmentResponseDto departmentHead;
    private List<ProfessorInDepartmentResponseDto> professors;
}
