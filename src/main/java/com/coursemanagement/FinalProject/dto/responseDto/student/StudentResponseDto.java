package com.coursemanagement.FinalProject.dto.responseDto.student;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentResponseDto {
    private List<CourseInStudentResponseDto> courses;
    private Long studentUniversityId;
    private String studentFirstName;
    private String studentLastName;
    private Long studentNationalId;
    @JsonIgnore
    private String password;
    private String address;
    private DepartmentInStudentResponseDto department;
}
