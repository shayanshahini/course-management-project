package com.coursemanagement.FinalProject.dto.responseDto.student;

import com.coursemanagement.FinalProject.dto.responseDto.department.ProfessorInDepartmentResponseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentInStudentResponseDto {
    private String departmentName;
    private ProfessorInStudentResponseDto departmentHead;
    private List<ProfessorInStudentResponseDto> professors;
}
