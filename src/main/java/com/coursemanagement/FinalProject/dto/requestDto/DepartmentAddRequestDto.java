package com.coursemanagement.FinalProject.dto.requestDto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

import java.util.List;

@Data
public class DepartmentAddRequestDto {
    @NotEmpty
    @Size(
            min = 5,
            max = 48,
            message = "Department name must be between 5 and 48 character"
    )
    private String departmentName;
    @NotNull
    private Long departmentHead;
    private List<Long> professors;
    private List<Long> students;
    @NotEmpty(message = "Course list must not be empty")
    private List<Long> courses;
}
