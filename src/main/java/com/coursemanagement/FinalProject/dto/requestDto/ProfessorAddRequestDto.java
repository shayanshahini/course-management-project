package com.coursemanagement.FinalProject.dto.requestDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.*;
import lombok.Data;

import java.util.List;

@Data
public class ProfessorAddRequestDto {
    @NotNull
    @Min(value = 10000, message = "Professor Personal Code must be greater than or equal to 10000")
    @Max(value = 99999, message = "Professor Personal Code must be less than or equal to 99999")
    private Long professorPersonalCode;

    @NotEmpty
    @Size(
            min = 1,
            max = 48,
            message = "Professor First Name must be less than or equal to 48 character"
    )
    private String professorFirstName;

    @NotEmpty
    @Size(
            min = 1,
            max = 48,
            message = "Professor Last Name must be less than or equal to 48 character"
    )
    private String professorLastName;

    @NotNull
    @Min(value = 10000, message = "Professor National Id must be greater than or equal to 10000")
    @Max(value = 99999, message = "Professor National Id must be less than or equal to 99999")
    private Long professorNationalId;

    @Size(
            min = 4,
            max = 8,
            message = "Professor password must be between 4 and 8 character."
    )
    private String password;
    private Long department;
    private List<Long> courses;
}
