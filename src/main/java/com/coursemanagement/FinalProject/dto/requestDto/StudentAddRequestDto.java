package com.coursemanagement.FinalProject.dto.requestDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.*;
import lombok.Data;

import java.util.HashMap;

@Data
public class StudentAddRequestDto {
    @NotNull
    @Min(value = 10000, message = "Student university id must be greater than or equal to 10000")
    @Max(value = 99999, message = "Student university id must be less than or equal to 99999")
    private Long studentUniversityId;

    @NotEmpty
    @Size(
            min = 1,
            max = 48,
            message = "Student first name must be less than or equal to 48 character"
    )
    private String studentFirstName;

    @NotEmpty
    @Size(
            min = 1,
            max = 48,
            message = "Student last name must be less than or equal to 48 character"
    )
    private String studentLastName;

    @NotNull
    @Min(value = 10000, message = "Student national id must be greater than or equal to 10000")
    @Max(value = 99999, message = "Student national id must be less than or equal to 99999")
    private Long studentNationalId;

    @Size(
            min = 4,
            max = 8,
            message = "Student password must be between 4 and 8 character."
    )
    private String password;

    @Size(
            min = 1,
            max = 128,
            message = "student address must be less than or equal to 128 character"
    )
    private String address;

    private Long department;

    @NotEmpty(message = "Enrollments list must not be empty")
    HashMap<Long, Double> courseWithGrade = new HashMap<>();
}
