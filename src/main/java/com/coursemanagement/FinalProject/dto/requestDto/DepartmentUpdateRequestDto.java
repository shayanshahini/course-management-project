package com.coursemanagement.FinalProject.dto.requestDto;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import lombok.Data;

import java.util.List;

@Data
public class DepartmentUpdateRequestDto {
    @Size(
            min = 5,
            max = 48,
            message = "Department name must be between 5 and 48 character"
    )
    private String departmentName;
    private Long departmentHead;
    private List<Long> professors;
    private List<Long> students;
    private List<Long> courses;
}
