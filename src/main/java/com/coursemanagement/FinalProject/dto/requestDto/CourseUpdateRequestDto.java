package com.coursemanagement.FinalProject.dto.requestDto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import lombok.Data;

import java.util.HashMap;

@Data
public class CourseUpdateRequestDto {
    @Size(
            min = 1,
            max = 64,
            message = "Course name must be less than or equal to 64 character"
    )
    private String courseName;
//    @Min(value = 1, message = "Course credit must be greater than or equal to 1")
//    @Max(value = 9, message = "Course credit must be less than or equal to 9")
    private int courseCredit;
    private Long department;
    private Long professor;
    HashMap<Long, Double> studentWithGrade = new HashMap<>();
}
