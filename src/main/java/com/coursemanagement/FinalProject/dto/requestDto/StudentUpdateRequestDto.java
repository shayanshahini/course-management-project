package com.coursemanagement.FinalProject.dto.requestDto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.Data;

import java.util.HashMap;

@Data
public class StudentUpdateRequestDto {
    @Min(value = 10000, message = "Student university id must be greater than or equal to 10000")
    @Max(value = 99999, message = "Student university id must be less than or equal to 99999")
    private Long studentUniversityId;
    @Size(
            min = 1,
            max = 48,
            message = "Student first name must be less than or equal to 48 character"
    )
    private String studentFirstName;
    @Size(
            min = 1,
            max = 48,
            message = "Student last name must be less than or equal to 48 character"
    )
    private String studentLastName;
    @Min(value = 10000, message = "Student national id must be greater than or equal to 10000")
    @Max(value = 99999, message = "Student national id must be less than or equal to 99999")
    private Long studentNationalId;
    @Size(
            min = 4,
            max = 8,
            message = "Student password must be between 4 and 8 character."
    )
    private String password;
    @Size(
            min = 1,
            max = 128,
            message = "student address must be less than or equal to 128 character"
    )
    private String address;
    private Long department;
    HashMap<Long, Double> courseWithGrade;
}
