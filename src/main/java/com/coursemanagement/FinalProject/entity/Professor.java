package com.coursemanagement.FinalProject.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import javax.validation.constraints.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Data
@NoArgsConstructor
@Entity(name = "Professor")
@Table(name = "professor")
public class Professor {

    //*****FIELDS*****//
    @Id
    @SequenceGenerator(
            name = "professor_id_sequence",
            sequenceName = "professor_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "professor_id_sequence"
    )
    @Column(updatable = false)
    private Long professorId;

    @Column(unique = true)
    @NotNull
    @Min(value = 10000, message = "Professor Personal Code must be greater than or equal to 10000")
    @Max(value = 99999, message = "Professor Personal Code must be less than or equal to 99999")
    private Long professorPersonalCode;

    @NotEmpty
    @Size(
            min = 1,
            max = 48,
            message = "Professor First Name must be less than or equal to 48 character"
    )
    private String professorFirstName;

    @NotEmpty
    @Size(
            min = 1,
            max = 48,
            message = "Professor Last Name must be less than or equal to 48 character"
    )
    private String professorLastName;

    @Column(unique = true)
    @NotNull
    @Min(value = 10000, message = "Professor National Id must be greater than or equal to 10000")
    @Max(value = 99999, message = "Professor National Id must be less than or equal to 99999")
    private Long professorNationalId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Column(updatable = false)
    private String role = "PROFESSOR";

    @OneToOne(
            fetch = FetchType.LAZY,
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
            mappedBy = "departmentHead"
    )
    private Department departmentHead;

    @ManyToOne(
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
            fetch = FetchType.LAZY
    )
    @JoinColumn(
            name = "department_id",
            referencedColumnName = "departmentId"
    )
    private Department department;
    @OneToMany(
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
            mappedBy = "professor",
            fetch = FetchType.LAZY
    )
    private List<Course> courses = new ArrayList<Course>();

    //*****CONSTRUCTORS*****//
   public Professor(Long professorPersonalCode, String professorFirstName, String professorLastName, Long professorNationalId, String password) {
        this.professorPersonalCode = professorPersonalCode;
        this.professorFirstName = professorFirstName;
        this.professorLastName = professorLastName;
        this.professorNationalId = professorNationalId;
        this.password = password;
    }

    //*****CONVENIENCE*****//
    public void addCourse(Course course) {
        if (!courses.contains(course)) {
            courses.add(course);
            course.setProfessor(this);
        }
    }

    public void removeCourse(Course course) {
        if (courses.contains(course)) {
            courses.remove(course);
            course.setProfessor(null);
        }
    }

    //*****ToSTRING*****//

    @Override
    public String toString() {
        return "Professor{" +
                "professorId=" + professorId +
                ", professorPersonalCode=" + professorPersonalCode +
                ", professorFirstName='" + professorFirstName + '\'' +
                ", professorLastName='" + professorLastName + '\'' +
                ", professorNationalId=" + professorNationalId +
                '}';
    }
}
