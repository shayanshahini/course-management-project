package com.coursemanagement.FinalProject.entity;

import com.coursemanagement.FinalProject.entity.utility.EnrollmentId;
import javax.persistence.*;
import javax.validation.constraints.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "enrollment")
public class Enrollment {

    @NotNull
    @EmbeddedId
    private EnrollmentId enrollmentId;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @MapsId("studentId")
    @JoinColumn(
            name = "student_id",
    foreignKey = @ForeignKey(name = "student_enrollment_fk"),
    nullable = false)
    private Student student;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @MapsId("courseId")
    @JoinColumn(
            name = "course_id",
    foreignKey = @ForeignKey(name = "course_enrollment_fk"),
    nullable = false)
    private Course course;

    @NotNull
    @Min(value = 0, message = "Grade must be greater than or equal to 0")
    @Max(value = 20, message = "Grade must be less than or equal to 20")
    private Double grade;

    public Enrollment(EnrollmentId enrollmentId,
                      Student student,
                      Course course,
                      double grade) {
        this.enrollmentId = enrollmentId;
        this.student = student;
        this.course = course;
        this.grade = grade;
    }

    public Enrollment(Student student,
                      Course course,
                      double grade) {
        this.student = student;
        this.course = course;
        this.grade = grade;
    }

}
