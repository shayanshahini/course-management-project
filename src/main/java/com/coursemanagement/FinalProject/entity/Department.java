package com.coursemanagement.FinalProject.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Data
@NoArgsConstructor
@Entity(name = "Department")
@Table(
        name = "department",
        uniqueConstraints = {
                @UniqueConstraint(name = "department_name", columnNames = "departmentName")
        }
)
public class Department {

    //*****FIELDS*****//
    @Id
    @SequenceGenerator(
            name = "department_id_sequence",
            sequenceName = "department_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "department_id_sequence"
    )
    private Long departmentId;

    @NotEmpty
    @Size(
            min = 5,
            max = 48,
            message = "Department name must be between 5 and 48 character"
    )
    private String departmentName;

    @OneToOne(
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
            fetch = FetchType.LAZY
    )
    @JoinColumn(
            name = "professor_head_id",
            referencedColumnName = "professorId",
            nullable = false
    )
    private Professor departmentHead;
    @OneToMany(
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
            mappedBy = "department",
            fetch = FetchType.LAZY
    )
    private List<Professor> professors = new ArrayList<Professor>();
    @OneToMany(
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
            mappedBy = "department",
            fetch = FetchType.LAZY
    )
    private List<Student> students = new ArrayList<Student>();

    @NotEmpty(message = "Course list must not be empty")
    @OneToMany(
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
            mappedBy = "department",
            fetch = FetchType.LAZY
    )
    private List<Course> courses = new ArrayList<Course>();

    //*****CONSTRUCTORS*****//
    public Department(String departmentName) {
        this.departmentName = departmentName;
    }

    //*****CONVENIENCE*****//
    public void addProfessor(Professor professor) {
        if (!professors.contains(professor)) {
            professors.add(professor);
            professor.setDepartment(this);
        }
    }

    public void removeProfessor(Professor professor) {
        if (professors.contains(professor)) {
            professors.remove(professor);
            professor.setDepartment(null);
        }
    }

    public void addStudent(Student student) {
        if (!students.contains(student)) {
            students.add(student);
            student.setDepartment(this);
        }
    }

    public void removeStudent(Student student) {
        if (students.contains(student)) {
            students.remove(student);
            student.setDepartment(null);
        }
    }

    public void addCourse(Course course) {
        if (!courses.contains(course)) {
            courses.add(course);
            course.setDepartment(this);
        }
    }

    public void removeCourse(Course course) {
        if (courses.contains(course)) {
            courses.remove(course);
            course.setDepartment(null);
        }
    }

    //*****ToSTRING*****//

    @Override
    public String toString() {
        return "Department{" +
                "departmentId=" + departmentId +
                ", departmentName='" + departmentName + '\'' +
                '}';
    }
}
