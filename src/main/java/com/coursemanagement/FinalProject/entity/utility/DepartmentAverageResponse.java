package com.coursemanagement.FinalProject.entity.utility;

public class DepartmentAverageResponse {
    private double average;

    public DepartmentAverageResponse(double average) {
        this.average = average;
    }

    public double getAverage() {
        return average;
    }
}
