package com.coursemanagement.FinalProject.entity.utility;

public class StudentAverageResponse {
    private double average;

    public StudentAverageResponse(double average) {
        this.average = average;
    }

    public double getAverage() {
        return average;
    }
}
