package com.coursemanagement.FinalProject.entity.utility;

public class ProfessorAverageResponse {
    private double average;

    public ProfessorAverageResponse(double average) {
        this.average = average;
    }

    public double getAverage() {
        return average;
    }
}
