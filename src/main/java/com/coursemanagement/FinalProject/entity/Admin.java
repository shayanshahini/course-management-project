package com.coursemanagement.FinalProject.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import javax.validation.constraints.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import static javax.persistence.GenerationType.SEQUENCE;

@Data
@NoArgsConstructor
@Entity(name = "Admin")
public class Admin {

    //*****FIELDS*****//
    @Id
    @SequenceGenerator(
            name = "admin_id_sequence",
            sequenceName = "admin_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "admin_id_sequence"
    )
    @Column(updatable = false)
    private Long adminId;

    @NotEmpty
    @Size(
            min = 1,
            max = 48,
            message = "Admin first name must be less than or equal to 48 character"
    )
    private String adminFirstName;
    @NotEmpty
    @Size(
            min = 1,
            max = 48,
            message = "Admin last name must be less than or equal to 48 character"
    )
    private String adminLastName;

    @NotNull
    @Min(value = 10000, message = "Admin national id must be greater than or equal to 10000")
    @Max(value = 99999, message = "Admin national id must be less than or equal to 99999")
    @Column(unique = true)
    private Long adminNationalId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Column(updatable = false)
    @JsonIgnore
    private final String role = "ADMIN";


    //*****CONSTRUCTORS*****//
    public Admin(String adminFirstName, String adminLastName, Long adminNationalId, String password) {
        this.adminFirstName = adminFirstName;
        this.adminLastName = adminLastName;
        this.adminNationalId = adminNationalId;
        this.password = password;
    }

    //*****ToSTRING*****//
    @Override
    public String toString() {
        return "Admin{" +
                "adminFirstName='" + adminFirstName + '\'' +
                ", adminLastName='" + adminLastName + '\'' +
                ", adminNationalId=" + adminNationalId +
                '}';
    }
}
