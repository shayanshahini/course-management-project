package com.coursemanagement.FinalProject.entity;

import javax.persistence.*;
import javax.transaction.Transactional;
import javax.validation.constraints.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Data
@NoArgsConstructor
@Entity(name = "Course")
@Table(
        name = "course",
        uniqueConstraints = {
                @UniqueConstraint(name = "course_name", columnNames = "courseName")
        }
)
public class Course {

    //*****FIELDS*****//
    @Id
    @SequenceGenerator(
            name = "course_id_sequence",
            sequenceName = "course_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "course_id_sequence"
    )
    private Long courseId;

    @NotEmpty
    @Size(
            min = 1,
            max = 64,
            message = "Course name must be less than or equal to 64 character"
    )
    private String courseName;

    @NotNull
    @Min(value = 1, message = "Course credit must be greater than or equal to 1")
    @Max(value = 9, message = "Course credit must be less than or equal to 9")
    private int courseCredit;
    @ManyToOne(
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
            fetch = FetchType.LAZY
    )
    @JoinColumn(
            name = "department_id",
            referencedColumnName = "departmentId"
    )
    private Department department;
    @ManyToOne(
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
            fetch = FetchType.LAZY
    )
    @JoinColumn(
            name = "professor_id",
            referencedColumnName = "professorId",
            foreignKey = @ForeignKey(name = "professor_FK")
    )
    private Professor professor;

    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "course"
    )
    List<Enrollment> enrollments = new ArrayList<>();

    //*****CONSTRUCTORS*****//
    public Course(String courseName, int courseCredit) {
        this.courseName = courseName;
        this.courseCredit = courseCredit;
    }

    //*****CONVENIENCE*****//
    public void addEnrollment(Enrollment enrollment) {
        if (!enrollments.contains(enrollment)) {
            enrollments.add(enrollment);
            enrollment.setCourse(this);
        }
    }

    @Transactional
    public void removeEnrollment(Enrollment enrollment) {
        if (enrollments.contains(enrollment)) {
            enrollments.remove(enrollment);
        }
    }

    //*****ToSTRING*****//
    @Override
    public String toString() {
        return "Course{" +
                "courseId=" + courseId +
                ", courseName='" + courseName + '\'' +
                ", courseCredit=" + courseCredit +
                '}';
    }

}
