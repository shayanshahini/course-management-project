package com.coursemanagement.FinalProject.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import javax.transaction.Transactional;
import javax.validation.constraints.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Data
@NoArgsConstructor
@Entity(name = "Students")
@Table(
        name = "student",
        uniqueConstraints = {
                @UniqueConstraint(name = "student_university_id", columnNames = "studentUniversityId")
        }
)
public class Student {

    //*****FIELDS*****//
    @Id
    @SequenceGenerator(
            name = "student_id_sequence",
            sequenceName = "student_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "student_id_sequence"
    )
    @Column(updatable = false)
    private Long studentId;

    @NotNull
    @Min(value = 10000, message = "Student university id must be greater than or equal to 10000")
    @Max(value = 99999, message = "Student university id must be less than or equal to 99999")
    @Column(unique = true)
    private Long studentUniversityId;

    @NotEmpty
    @Size(
            min = 1,
            max = 48,
            message = "Student first name must be less than or equal to 48 character"
    )
    private String studentFirstName;
    @NotEmpty
    @Size(
            min = 1,
            max = 48,
            message = "Student last name must be less than or equal to 48 character"
    )
    private String studentLastName;

    @NotNull
    @Min(value = 10000, message = "Student national id must be greater than or equal to 10000")
    @Max(value = 99999, message = "Student national id must be less than or equal to 99999")
    @Column(unique = true)
    private Long studentNationalId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Column(updatable = false)
    @JsonIgnore
    private final String role = "STUDENT";

    @Size(
            min = 1,
            max = 128,
            message = "student address must be less than or equal to 128 character"
    )
    private String address;

    @ManyToOne(
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
            fetch = FetchType.LAZY
    )
    @JoinColumn(
            name = "department_id",
            referencedColumnName = "departmentId"
    )
    private Department department;

    @NotEmpty(message = "Enrollments list must not be empty")
    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "student"
    )
    private List<Enrollment> enrollments = new ArrayList<Enrollment>();

    //*****CONSTRUCTORS*****//
    public Student(Long studentUniversityId, String studentFirstName, String studentLastName, Long studentNationalId, String password, String address) {
        this.studentUniversityId = studentUniversityId;
        this.studentFirstName = studentFirstName;
        this.studentLastName = studentLastName;
        this.studentNationalId = studentNationalId;
        this.password = password;
        this.address = address;
    }

    //*****CONVENIENCE*****//
    public void addEnrollment(Enrollment enrollment) {
        if (!enrollments.contains(enrollment)) {
            enrollments.add(enrollment);
            enrollment.setStudent(this);
        }
    }

    public void removeEnrollment(Enrollment enrollment) {
        if (enrollments.contains(enrollment)) {
            enrollments.remove(enrollment);
        }
    }

    //*****ToSTRING*****//

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", studentUniversityId=" + studentUniversityId +
                ", studentFirstName='" + studentFirstName + '\'' +
                ", studentLastName='" + studentLastName + '\'' +
                ", studentNationalId=" + studentNationalId +
                ", address='" + address + '\'' +
                '}';
    }
}
