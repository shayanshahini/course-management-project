package com.coursemanagement.FinalProject.controller;

import com.coursemanagement.FinalProject.entity.utility.DepartmentAverageResponse;
import com.coursemanagement.FinalProject.service.DepartmentServiceImp;
import com.coursemanagement.FinalProject.dto.requestDto.DepartmentAddRequestDto;
import com.coursemanagement.FinalProject.dto.requestDto.DepartmentUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.department.DepartmentResponseDto;
import com.coursemanagement.FinalProject.entity.Department;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/departments")
//@CrossOrigin("*")
@Validated
public class DepartmentController {
    private DepartmentServiceImp departmentServiceImp;

    @Autowired
    public DepartmentController(DepartmentServiceImp departmentServiceImp) {
        this.departmentServiceImp = departmentServiceImp;
    }

    @GetMapping("/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<DepartmentResponseDto> getDepartment(@PathVariable Long id) {
        DepartmentResponseDto departmentResponseDto = departmentServiceImp.getDepartment(id);
        return new ResponseEntity<>(departmentResponseDto, HttpStatus.OK);
    }
    //Without pagination
//    @GetMapping("/all")
////    @PreAuthorize("hasAuthority('ADMIN')")
//    public ResponseEntity<List<DepartmentResponseDto>> getDepartment() {
//        List<DepartmentResponseDto> departmentResponseDtos = departmentServiceImp.getDepartments();
//        return new ResponseEntity<>(departmentResponseDtos, HttpStatus.OK);
//    }

    @GetMapping()
    public ResponseEntity<List<DepartmentResponseDto>> getDepartmentsWithPagination(
            @RequestParam(defaultValue = "0") int offset,
            @RequestParam(defaultValue = "2") int size
    ) {
        List<DepartmentResponseDto> departmentsWithPagination = departmentServiceImp.getDepartmentsWithPagination(offset, size);
        return new ResponseEntity<>(departmentsWithPagination, HttpStatus.OK);
    }

    @PostMapping()
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<DepartmentResponseDto> addDepartment(@RequestBody @Valid DepartmentAddRequestDto departmentAddRequestDto) {
        DepartmentResponseDto departmentResponseDto;
        departmentResponseDto = departmentServiceImp.addDepartment(departmentAddRequestDto);
        return new ResponseEntity<>(departmentResponseDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Department> deleteById(@PathVariable Long id) {
        Department department = departmentServiceImp.deleteById(id);
        return new ResponseEntity<>(department ,HttpStatus.OK);
    }

    @PutMapping("/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<DepartmentResponseDto> updateDepartment(
            @RequestBody @Valid DepartmentUpdateRequestDto departmentUpdateRequestDto,
            @PathVariable Long id) {
        DepartmentResponseDto departmentResponseDto;
        departmentResponseDto = departmentServiceImp.updateDepartment(id, departmentUpdateRequestDto);
        return new ResponseEntity<>(departmentResponseDto, HttpStatus.OK);
    }

    @PutMapping("/{departmentId}/head/{professorHeadId}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<DepartmentResponseDto> chooseDepartmentHead(
            @PathVariable Long departmentId,
            @PathVariable Long professorHeadId) {
        DepartmentResponseDto departmentResponseDto;
        departmentResponseDto = departmentServiceImp.chooseDepartmentHead(departmentId, professorHeadId);
        return new ResponseEntity<>(departmentResponseDto, HttpStatus.OK);
    }

    @PutMapping("/{departmentId}/professors/{professorId}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<DepartmentResponseDto> chooseProfessor(
            @PathVariable Long departmentId,
            @PathVariable Long professorId) {
        DepartmentResponseDto departmentResponseDto;
        departmentResponseDto = departmentServiceImp.chooseProfessor(departmentId, professorId);
        return new ResponseEntity<>(departmentResponseDto, HttpStatus.OK);
    }

    @GetMapping("/{id}/average")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<DepartmentAverageResponse> departmentAverage(@PathVariable Long id) {
        double average = departmentServiceImp.departmentAverage(id);
        DepartmentAverageResponse response = new DepartmentAverageResponse(average);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
