package com.coursemanagement.FinalProject.controller;

import com.coursemanagement.FinalProject.dto.responseDto.student.StudentResponseDto;
import com.coursemanagement.FinalProject.service.CourseServiceImp;
import com.coursemanagement.FinalProject.dto.requestDto.CourseAddRequestDto;
import com.coursemanagement.FinalProject.dto.requestDto.CourseUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.course.CourseResponseDto;
import com.coursemanagement.FinalProject.entity.Course;
import com.coursemanagement.FinalProject.repository.CourseRepository;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/courses")
//@CrossOrigin("*")

@Valid
public class CourseController {
    private CourseServiceImp courseServiceImp;

    @Autowired
    public CourseController(CourseServiceImp courseServiceImp) {
        this.courseServiceImp = courseServiceImp;
    }

    @GetMapping("/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CourseResponseDto> getCourse(@PathVariable Long id) {
        CourseResponseDto courseResponseDto = courseServiceImp.getCourse(id);
        return new ResponseEntity<>(courseResponseDto, HttpStatus.OK);
    }

//    @GetMapping("/getAll")
////    @PreAuthorize("hasAuthority('ADMIN')")
//    public ResponseEntity<List<CourseResponseDto>> getCourses() {
//        List<CourseResponseDto> courseResponseDtos = courseServiceImp.getCourses();
//        return new ResponseEntity<>(courseResponseDtos, HttpStatus.OK);
//    }

    @GetMapping()
    public ResponseEntity<List<CourseResponseDto>> getCoursesWithPagination(
            @RequestParam(defaultValue = "0") int offset,
            @RequestParam(defaultValue = "2") int size
    ) {
        List<CourseResponseDto> coursesWithPagination = courseServiceImp.getCoursesWithPagination(offset, size);
        return new ResponseEntity<>(coursesWithPagination, HttpStatus.OK);
    }

    @PostMapping()
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CourseResponseDto> addCourse(@RequestBody @Valid CourseAddRequestDto courseAddRequestDto) {
        CourseResponseDto courseResponseDto;
        courseResponseDto = courseServiceImp.addCourse(courseAddRequestDto);
        return new ResponseEntity<>(courseResponseDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Course> deleteById(@PathVariable Long id) {
        Course course = courseServiceImp.deleteById(id);
        return new ResponseEntity<>(course, HttpStatus.OK);
    }

    @PutMapping("/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CourseResponseDto> updateCourse(
            @RequestBody @Valid CourseUpdateRequestDto courseUpdateRequestDto,
            @PathVariable Long id) {
        CourseResponseDto courseResponseDto;
        courseResponseDto = courseServiceImp.updateCourse(id, courseUpdateRequestDto);
        return new ResponseEntity<>(courseResponseDto, HttpStatus.OK);
    }

    @PutMapping("/{courseId}/students/{studentId}/grade/{grade}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CourseResponseDto> chooseStudent(
            @PathVariable Long courseId,
            @PathVariable Long studentId,
            @PathVariable Double grade
    ) {
        CourseResponseDto courseResponseDto;
        courseResponseDto = courseServiceImp.chooseStudent(courseId, studentId, grade);
        return new ResponseEntity<>(courseResponseDto, HttpStatus.OK);
    }
}
