package com.coursemanagement.FinalProject.controller;

import com.coursemanagement.FinalProject.dto.responseDto.professor.ProfessorResponseDto;
import com.coursemanagement.FinalProject.entity.utility.StudentAverageResponse;
import com.coursemanagement.FinalProject.service.StudentServiceImp;
import com.coursemanagement.FinalProject.dto.requestDto.StudentAddRequestDto;
import com.coursemanagement.FinalProject.dto.requestDto.StudentUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.student.StudentResponseDto;
import com.coursemanagement.FinalProject.entity.Student;
import com.coursemanagement.FinalProject.repository.StudentRepository;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/students")
//@CrossOrigin("*")
@Validated
public class StudentController {
    private StudentServiceImp studentServiceImp;

    @Autowired
    public StudentController(StudentServiceImp studentServiceImp) {
        this.studentServiceImp = studentServiceImp;
    }

    @GetMapping("/{studentNationalId}")
//    @PreAuthorize("#studentNationalId.toString() == authentication.name || hasAuthority('ADMIN')")
//    @PreAuthorize("#id.toString() == studentRepository.findByStudentNationalId(Long.parseLong(authentication.name)).get(0).getStudentId().toString()")
    public ResponseEntity<StudentResponseDto> getStudentByNationalId(@PathVariable Long studentNationalId) {
        StudentResponseDto studentResponseDto = studentServiceImp.getStudentByNationalId(studentNationalId);
        return new ResponseEntity<>(studentResponseDto, HttpStatus.OK);
    }

//    @GetMapping("/getAll")
////    @PreAuthorize("hasAuthority('ADMIN')")
//    public ResponseEntity<List<StudentResponseDto>> getStudents() {
//        List<StudentResponseDto> studentResponseDtos = studentServiceImp.getStudents();
//        return new ResponseEntity<>(studentResponseDtos, HttpStatus.OK);
//    }

    @GetMapping()
    public ResponseEntity<List<StudentResponseDto>> getProfessorsWithPagination(
            @RequestParam(defaultValue = "0") int offset,
            @RequestParam(defaultValue = "2") int size
    ) {
        List<StudentResponseDto> studentsWithPagination = studentServiceImp.getStudentsWithPagination(offset, size);
        return new ResponseEntity<>(studentsWithPagination, HttpStatus.OK);
    }

    @PostMapping()
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<StudentResponseDto> addStudent(@RequestBody @Valid StudentAddRequestDto studentAddRequestDto) {
        StudentResponseDto studentResponseDto;
        studentResponseDto = studentServiceImp.addStudent(studentAddRequestDto);
        return new ResponseEntity<>(studentResponseDto, HttpStatus.OK);
    }

    @DeleteMapping("{studentNationalId}")
//    @PreAuthorize("hasAuthority('ADMIN')")
//    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('STUDENT')")
    public ResponseEntity<Student> deleteByNationalId(@PathVariable Long studentNationalId) {
        Student student = studentServiceImp.deleteByNationalId(studentNationalId);
        return new ResponseEntity<>(student, HttpStatus.OK);
    }

//    @PutMapping("update/{studentNationalId}")
    @PutMapping("{studentNationalId}")
//    @PreAuthorize("#studentNationalId.toString() == authentication.name || hasAuthority('ADMIN')")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<StudentResponseDto> updateStudentByNationalId(
            @RequestBody @Valid StudentUpdateRequestDto studentUpdateRequestDto,
            @PathVariable Long studentNationalId) {
        StudentResponseDto studentResponseDto;
        studentResponseDto = studentServiceImp.updateStudentByNationalId(studentNationalId, studentUpdateRequestDto);
        return new ResponseEntity<>(studentResponseDto, HttpStatus.OK);
    }

    @PutMapping("/{studentNationalId}/courses/{courseId}/grade/{grade}")
//    @PreAuthorize("hasAuthority('ADMIN')")
//    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('STUDENT')")
    public ResponseEntity<StudentResponseDto> chooseCourse(
            @PathVariable Long studentNationalId,
            @PathVariable Long courseId,
            @PathVariable Double grade
    ) {
        StudentResponseDto studentResponseDto;
        studentResponseDto = studentServiceImp.chooseCourseByStudentNationalId(studentNationalId, courseId, grade);
        return new ResponseEntity<>(studentResponseDto, HttpStatus.OK);
    }

//    @GetMapping("/studentAverage/studentNationalId/{studentNationalId}")
    @GetMapping("{studentNationalId}/average")
//    @PreAuthorize("#studentNationalId.toString() == authentication.name || hasAuthority('ADMIN')")
//    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('STUDENT')")
    public ResponseEntity<StudentAverageResponse> studentAverageByNationalId(@PathVariable Long studentNationalId) {
        double average = studentServiceImp.studentAverageByNationalId(studentNationalId);
        StudentAverageResponse response = new StudentAverageResponse(average);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
