package com.coursemanagement.FinalProject.controller;

import com.coursemanagement.FinalProject.dto.responseDto.department.DepartmentResponseDto;
import com.coursemanagement.FinalProject.entity.utility.ProfessorAverageResponse;
import com.coursemanagement.FinalProject.entity.utility.StudentAverageResponse;
import com.coursemanagement.FinalProject.service.ProfessorServiceImp;
import com.coursemanagement.FinalProject.dto.requestDto.ProfessorAddRequestDto;
import com.coursemanagement.FinalProject.dto.requestDto.ProfessorUpdateRequestDto;
import com.coursemanagement.FinalProject.dto.responseDto.professor.ProfessorResponseDto;
import com.coursemanagement.FinalProject.entity.Professor;
import com.coursemanagement.FinalProject.repository.ProfessorRepository;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/professors")
//@CrossOrigin("*")

@Validated
public class ProfessorController {
    private ProfessorServiceImp professorServiceImp;

    @Autowired
    public ProfessorController(ProfessorServiceImp professorServiceImp) {
        this.professorServiceImp = professorServiceImp;
    }

    @GetMapping("/{professorNationalId}")
//    @PreAuthorize("#professorNationalId.toString() == authentication.name || hasAuthority('ADMIN')")
    public ResponseEntity<ProfessorResponseDto> getProfessorByNationalID(@PathVariable Long professorNationalId) {
        ProfessorResponseDto professorResponseDto = professorServiceImp.getProfessorByNationalId(professorNationalId);
        return new ResponseEntity<>(professorResponseDto, HttpStatus.OK);
    }

//    @GetMapping("/getAll")
////    @PreAuthorize("hasAuthority('ADMIN')")
//    public ResponseEntity<List<ProfessorResponseDto>> getProfessors() {
//        List<ProfessorResponseDto> professorResponseDtos = professorServiceImp.getProfessors();
//        return new ResponseEntity<>(professorResponseDtos, HttpStatus.OK);
//    }

    @GetMapping()
    public ResponseEntity<List<ProfessorResponseDto>> getProfessorsWithPagination(
            @RequestParam(defaultValue = "0") int offset,
            @RequestParam(defaultValue = "2") int size
    ) {
        List<ProfessorResponseDto> professorsWithPagination = professorServiceImp.getProfessorsWithPagination(offset, size);
        return new ResponseEntity<>(professorsWithPagination, HttpStatus.OK);
    }

    @PostMapping()
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ProfessorResponseDto> addProfessor(@RequestBody @Valid ProfessorAddRequestDto professorAddRequestDto) {
        ProfessorResponseDto professorResponseDto;
        professorResponseDto = professorServiceImp.addProfessor(professorAddRequestDto);
        return new ResponseEntity<>(professorResponseDto, HttpStatus.OK);
    }

    @DeleteMapping("/{professorNationalId}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Professor> deleteById(@PathVariable Long professorNationalId) {
        Professor professor = professorServiceImp.deleteByIdNationalId(professorNationalId);
        return new ResponseEntity<>(professor ,HttpStatus.OK);
    }
    @PutMapping("/{professorNationalId}")
//    @PreAuthorize("#professorNationalId.toString() == authentication.name || hasAuthority('ADMIN')")
    public ResponseEntity<ProfessorResponseDto> updateStudent(
            @RequestBody @Valid ProfessorUpdateRequestDto professorUpdateRequestDto,
            @PathVariable Long professorNationalId) {
        ProfessorResponseDto professorResponseDto;
        professorResponseDto = professorServiceImp.updateProfessorByNationalId(professorNationalId, professorUpdateRequestDto);
        return new ResponseEntity<>(professorResponseDto, HttpStatus.OK);
    }

    @PutMapping("{professorNationalId}/courses/{courseId}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ProfessorResponseDto> chooseCourse(
            @PathVariable Long professorNationalId,
            @PathVariable Long courseId
    ) {
        ProfessorResponseDto professorResponseDto;
        professorResponseDto = professorServiceImp.chooseCourseByProfessorNationalId(professorNationalId, courseId);
        return new ResponseEntity<>(professorResponseDto, HttpStatus.OK);
    }

    @GetMapping("/{professorNationalId}/courses/{courseId}/average")
//    @PreAuthorize("#professorNationalId.toString() == authentication.name || hasAuthority('ADMIN')")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ProfessorAverageResponse> courseAverage(
            @PathVariable Long professorNationalId,
            @PathVariable Long courseId) {
        double average = professorServiceImp.courseAverageByProfessorNationalId(professorNationalId, courseId);
        ProfessorAverageResponse response = new ProfessorAverageResponse(average);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
