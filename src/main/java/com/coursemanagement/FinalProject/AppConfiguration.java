package com.coursemanagement.FinalProject;

import com.coursemanagement.FinalProject.service.CourseServiceImp;
import com.coursemanagement.FinalProject.service.DepartmentServiceImp;
import com.coursemanagement.FinalProject.service.ProfessorServiceImp;
import com.coursemanagement.FinalProject.service.StudentServiceImp;
import com.coursemanagement.FinalProject.entity.*;
import com.coursemanagement.FinalProject.entity.utility.EnrollmentId;
import com.coursemanagement.FinalProject.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration()
public class AppConfiguration {

//    @Autowired
//    PasswordEncoder passwordEncoder;
    @Bean
    CommandLineRunner commandLineRunner(
            DepartmentRepository departmentRepository,
            ProfessorRepository professorRepository,
            StudentRepository studentRepository,
            CourseRepository courseRepository,
            DepartmentServiceImp departmentServiceImp,
            CourseServiceImp courseServiceImp,
            StudentServiceImp studentServiceImp,
            ProfessorServiceImp professorServiceImp,
            AdminRepository adminRepository) {
        return args -> {
            Department department1 = new Department("Mathematics");
            Department department2 = new Department("Physics");

            Professor professor1 = new Professor(87777L, "mehdi", "yaghoobi", 87787L, "3333");
//            professor1.setPassword(passwordEncoder.encode(professor1.getPassword()));
            Professor professor2 = new Professor(77966L, "reza", "yaghoobi", 77998L, "4444");
//            professor2.setPassword(passwordEncoder.encode(professor2.getPassword()));

            Student student1 = new Student(87795L, "shayan", "shahini", 88888L, "2222","gorgan");
//            student1.setPassword(passwordEncoder.encode(student1.getPassword()));
            Student student2 = new Student(88998L, "ali", "shaini", 99999L, "1111","gorgan");
//            student2.setPassword(passwordEncoder.encode(student2.getPassword()));

            Admin admin1 = new Admin("mostafa", "rezvani", 12332L, "9999");
//            admin1.setPassword(passwordEncoder.encode(admin1.getPassword()));

            Course course1 = new Course("web", 6);
            Course course2 = new Course("java", 9);

            student1.addEnrollment(
                    new Enrollment(
                            new EnrollmentId(student1.getStudentId(), course1.getCourseId()),
                            student1,
                            course1,
                            19.5
                    )
            );

            student2.addEnrollment(
                    new Enrollment(
                            new EnrollmentId(student2.getStudentId(), course2.getCourseId()),
                            student2,
                            course2,
                            19
                    )
            );

            course1.setProfessor(professor1);
            course2.setProfessor(professor1);
            department1.setDepartmentHead(professor1);
            department1.addProfessor(professor1);
            department1.addProfessor(professor2);
            department1.addCourse(course1);
            department1.addCourse(course2);
            department1.addStudent(student1);
            department1.addStudent(student2);

            departmentRepository.save(department1);
            adminRepository.save(admin1);
        };
    }
}
